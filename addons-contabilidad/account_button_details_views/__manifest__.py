{
	'name': 'Trazabilidad en Contabilidad-Botónes Accesos Directos varios en Contabilidad',
	'version': "1.0.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends': ['account','stock','account_bank_statement_automatic',
		'l10n_pe_detraccion','l10n_pe_retentions'],
	'description':'''
		Trazabilidad en Contabilidad. Mejora de vistas y Botones de acceso directo en extractos bancarios y contabilidad.
			> Trazabilidad en Contabilidad. Mejora vistas extractos bancarios y botones accesos directos en contabilidad
		''',
	'data':[
		'views/account_move_and_move_line_view.xml',
		'views/account_bank_statement_view.xml',
	],
	'installable': True,
	'auto_install': False,
}