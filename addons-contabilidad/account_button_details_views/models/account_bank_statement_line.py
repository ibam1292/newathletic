# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import logging
_logger = logging.getLogger(__name__)

class AccountBankStatementLine(models.Model):
	_inherit = "account.bank.statement.line"


	def get_account_move_ids(self):
		if 'move_line_id' in self._fields and self.move_line_id and self.move_line_id.move_id:
			return list(self.move_line_id.mapped('move_id.id'))
		else:
			return False 


	def get_account_payment_ids(self):
		if 'move_line_id' in self._fields and self.move_line_id and self.move_line_id.payment_id:
			return list(self.move_line_id.mapped('payment_id.id'))
		else:
			return False 

		

	def button_view_origin_account_payment(self):
		return {
			'name': 'Pago/Cobro',
			'view_mode': 'tree,form',
			'res_model': 'account.payment',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in', self.get_account_payment_ids() or [])],
			'context': {
				'company_id': self.company_id.id,}
		}

	
	def button_view_origin_account_move(self):
		return {
			'name': 'Asiento Contable',
			'view_mode': 'tree,form',
			'res_model': 'account.move',
			'view_id': False,
			'type': 'ir.actions.act_window',
			'domain': [('id', 'in',self.get_account_move_ids() or [])],
			'context': {
				'company_id': self.company_id.id,}
		}