# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError
import logging
_logger=logging.getLogger(__name__)

class AccountMove(models.Model):
	_inherit = 'account.move'


	exist_account_payment_id = fields.Boolean(string="Existe Pago/Cobro?",default=False,
		compute="compute_campo_exist_account_payment_id")

	exist_stock_move_id = fields.Boolean(string="Existe Movimiento de Stock?",default=False,
		compute="compute_campo_exist_stock_move_id")

	exist_stock_picking_id = fields.Boolean(string="Existe Orden de Transferencia?",default=False,
		compute="compute_campo_exist_stock_picking_id")

	exist_account_detraccion_id = fields.Boolean(string="Existe Registro Detracción?",default=False,
		compute="compute_campo_exist_account_detraccion_id")

	exist_account_retention_id = fields.Boolean(string="Existe Registro Retención?",default=False,
		compute="compute_campo_exist_account_retention_id")

	exist_account_transaction_id = fields.Boolean(string="Existe Transacción Caja/Banco?",default=False,
		compute="compute_campo_exist_account_transaction_id")

	#########################################################################################	

	def get_account_payment_ids(self):
		if 'payment_id' in self.line_ids._fields:
			return [i.id for i in self.line_ids.mapped('payment_id') if i]
		else:
			return None


	def get_stock_move_ids(self):
		if 'stock_move_id' in self._fields:
			return [i.id for i in self.mapped('stock_move_id') if i]
		else:
			return None


	def get_stock_picking_ids(self):
		if 'stock_move_id' in self._fields:
			return [i.id for i in self.mapped('stock_move_id.picking_id') if i]
		else:
			return None


	def get_account_detraccion_ids(self):
		if 'detraction_id' in self.line_ids._fields:
			return [i.id for i in self.line_ids.mapped('detraction_id') if i]
		else:
			return None


	def get_account_retention_ids(self):
		if 'invoice_retention_id' in self.line_ids._fields:
			return [i.id for i in self.line_ids.mapped('invoice_retention_id') if i]
		else:
			return None


	def get_account_transaction_ids(self):
		if 'statement_extract_line_id' in self.line_ids._fields:
			return [i.id for i in self.line_ids.mapped('statement_extract_line_id') if i]
		else:
			return None
			
	########################################################################################

	@api.depends('line_ids')		
	def compute_campo_exist_account_payment_id(self):
		for rec in self:
			rec.exist_account_payment_id = False
			if rec.get_account_payment_ids():
				rec.exist_account_payment_id = True
			

	@api.depends('stock_move_id')
	def compute_campo_exist_stock_move_id(self):
		for rec in self:
			rec.exist_stock_move_id = False
			if rec.get_stock_move_ids():
				rec.exist_stock_move_id = True
			

	@api.depends('stock_move_id')
	def compute_campo_exist_stock_picking_id(self):
		for rec in self:
			rec.exist_stock_picking_id = False
			if rec.get_stock_picking_ids():
				rec.exist_stock_picking_id = True


	@api.depends('line_ids')
	def compute_campo_exist_account_detraccion_id(self):
		for rec in self:
			rec.exist_account_detraccion_id = False
			if rec.get_account_detraccion_ids():
				rec.exist_account_detraccion_id = True


	@api.depends('line_ids')
	def compute_campo_exist_account_retention_id(self):
		for rec in self:
			rec.exist_account_retention_id = False
			if rec.get_account_retention_ids():
				rec.exist_account_retention_id = True


	@api.depends('line_ids')
	def compute_campo_exist_account_transaction_id(self):
		for rec in self:
			rec.exist_account_transaction_id = False
			if rec.get_account_transaction_ids():
				rec.exist_account_transaction_id = True	

	########################################################################################

	def button_view_account_payment(self):
		for rec in self:
			if rec.exist_account_payment_id:
				return {
					'name': 'Pago,Cobro o Transferencia',
					'view_mode': 'tree,form',
					'res_model': 'account.payment',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in', rec.get_account_payment_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}


	
	def button_view_account_detraccion(self):
		for rec in self:
			if rec.exist_account_detraccion_id:
				return {
					'name': 'Registro Detracción',
					'view_mode': 'tree,form',
					'res_model': 'account.detraction',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in',rec.get_account_detraccion_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}


	
	def button_view_account_transaction(self):
		for rec in self:
			if rec.exist_account_transaction_id:
				return {
					'name': 'Transacciones Bancarias',
					'view_mode': 'tree,form',
					'res_model': 'account.bank.statement.line',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in',rec.get_account_transaction_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}


	
	def button_view_account_retention(self):
		for rec in self:
			if rec.exist_account_retention_id:
				return {
					'name': 'Registro Retención',
					'view_mode': 'tree,form',
					'res_model': 'account.invoice.retention',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in',rec.get_account_retention_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}



	def button_view_stock_move(self):
		for rec in self:
			if rec.exist_stock_move_id:
				return {
					'name': 'Movimiento de Stock',
					'view_mode': 'tree,form',
					'res_model': 'stock.move',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in',rec.get_stock_move_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}


	
	def button_view_stock_picking(self):
		for rec in self:
			if rec.exist_stock_picking_id:
				return {
					'name': 'Orden Transferencia',
					'view_mode': 'tree,form',
					'res_model': 'stock.picking',
					'view_id': False,
					'type': 'ir.actions.act_window',
					'domain': [('id', 'in',rec.get_stock_picking_ids() or [])],
					'context': {
						'company_id': rec.company_id.id}}
