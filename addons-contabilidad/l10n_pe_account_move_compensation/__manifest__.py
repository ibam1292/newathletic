{
    'name': 'Registro de Compensación de Cuentas por Cobrar y Pagar.',
    'version': '1.0.0',
    'category': '',
    'license': 'AGPL-3',
    'summary': "Registro de Compensación de Cuentas por Cobrar y Pagar.",
    'author': "Franco Najarro",
    'website': '',
    'depends': ['account','gestionit_pe_fe','bo_pe_contabilidad_documents'],
    'data': [
        'security/ir.model.access.csv',
        'views/account_move_compensation_view.xml',
        ],
    'installable': True,
    'autoinstall': False,
}
