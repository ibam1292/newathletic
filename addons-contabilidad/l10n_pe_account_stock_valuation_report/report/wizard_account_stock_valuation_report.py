from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo import tools
from odoo.exceptions import UserError , ValidationError


options=[
    ('in','esta en'),
    ('not in','no esta en')]

class WizardAccountStockValuationReport(models.TransientModel):
    _name = "wizard.account.stock.valuation.report"
    _description = "Reporte Kardex Valorizado-Unidades"

    #######################################
    company_id = fields.Many2one('res.company',
        string="Compañia", 
        default=lambda self: self.env['res.company']._company_default_get('account.invoice'),
        domain = lambda self: [('id', 'in',[i.id for i in self.env['res.users'].browse(self.env.user.id).company_ids] )],readonly=True)

    ########################################### PARÁMETROS DE FILTRO DINÁMICO !!!
    fecha_final = fields.Datetime(string="Hasta Fecha",default=datetime.now())

    dinamic_warehouse_ids= fields.Many2many('stock.warehouse',string="Almacenes")
    warehouse_option=fields.Selection(selection=options , string="")

    dinamic_location_ids= fields.Many2many('stock.location',string="Ubicaciones Origen")
    location_option=fields.Selection(selection=options , string="")

    dinamic_product_ids= fields.Many2many('product.product',string="Productos")
    product_option=fields.Selection(selection=options , string="")


    valuation_report_line_ids = fields.One2many('account.stock.valuation.report',
        'wizard_valuation_report_id',string="Detalles de Reporte Kardex Valorizado")
    ################################################################################

    def get_query_kardex(self,filter_clause):

        query = """
            select 
                table_quantity.id as stock_valuation_layer_id,
                table_quantity.stock_move_id as move_id, 
                table_quantity.type_operation as TYPE, 
                table_quantity.account_move_id as account_move_id, 
                table_quantity.invoice_number as invoice_number, 
                table_quantity.origin as origin, 
                table_quantity.picking_id as picking_id, 
                table_quantity.purchase_line_id as purchase_line_id, 
                table_quantity.purchase_id as purchase_id, 
                table_quantity.sale_line_id as sale_line_id, 
                table_quantity.sale_id as sale_id, 
                table_quantity.product_id as product_id, 
                table_quantity.product_template_id as product_tmpl_id, 
                table_quantity.categ_id as categ_id, 
                table_quantity.uom_id as uom_id, 
                table_quantity.company_id as company_id, 
                table_quantity.quantity as quantity, 
                table_quantity.location_id as location_id, 
                table_quantity.usage as usage, 
                table_quantity.qty_in as qty_in, 
                table_quantity.cost_in as cost_in, 
                table_quantity.total_cost_in as total_cost_in, 
                table_quantity.qty_out as qty_out, 
                table_quantity.cost_out as cost_out, 
                table_quantity.total_cost_out as total_cost_out, 
                table_quantity.cantidad_actual as qty, 

                case when table_quantity.quantity < 0.00 then table_quantity.unit_cost
                when coalesce(table_quantity.cantidad_actual,0.00) = 0.00 then table_quantity.unit_cost 
                when coalesce(table_quantity.cantidad_actual,0.00) <> 0.00 
                then coalesce(table_value.valor_actual,0.00)/coalesce(table_quantity.cantidad_actual,0.00) end unit_cost,

                table_value.valor_actual as value, 
                table_quantity.create_date as date, 
                table_quantity.date_expected as date_expected, 
                table_quantity.picking_type_id as picking_type_id, 
                table_quantity.warehouse_id as warehouse_id, 
                table_quantity.inventory_id as inventory_id

                from 

                (select 
                svl.id, 
                svl.stock_move_id, 
                case when svl.value >=0.00 then 'in'
                else 'out' end type_operation,
                svl.account_move_id, 
                CASE 
                      WHEN sm.sale_line_id IS NOT NULL THEN
                      (
                      SELECT
                        array_to_string( ARRAY_AGG ( account_move.name ORDER BY account_move.name ), ', ' ) 
                      FROM
                        sale_order_line
                        INNER JOIN sale_order_line_invoice_rel ON sale_order_line_invoice_rel.order_line_id = sale_order_line.
                        ID INNER JOIN account_move_line ON sale_order_line_invoice_rel.invoice_line_id = account_move_line.
                        ID INNER JOIN account_move ON account_move_line.move_id = account_move.ID 
                      WHERE
                        sale_order_line.id = sm.sale_line_id 
                        AND account_move_line.product_id = sm.product_id  
                      ) 
                      WHEN sm.purchase_line_id IS NOT NULL THEN
                      (
                      SELECT
                        array_to_string( ARRAY_AGG ( account_move.name ORDER BY account_move.name ), ', ' ) 
                      FROM
                        purchase_order_line
                        INNER JOIN account_move_line ON account_move_line.purchase_line_id = purchase_order_line.
                        ID INNER JOIN account_move ON account_move_line.move_id = account_move.id 
                      WHERE
                        purchase_order_line.id = sm.purchase_line_id 
                        AND account_move_line.product_id = sm.product_id 
                      ) 
                      WHEN sm.inventory_id IS NOT NULL THEN
                      'AJUSTE' ELSE (
                      SELECT
                        array_to_string( ARRAY_AGG ( DISTINCT ( account_move.name ) ORDER BY account_move.name ), ', ' ) 
                      FROM
                        account_move
                        INNER JOIN account_move_line ON account_move_line.move_id = account_move.id 
                        INNER JOIN stock_picking ON stock_picking.origin = account_move.name 
                      WHERE
                        stock_picking.ID = sm.picking_id 
                        AND account_move_line.product_id = sm.product_id 
                      ) 
                    END AS invoice_number,
                sm.origin, 
                sm.picking_id, 
                sm.purchase_line_id, 
                pol.order_id as purchase_id, 
                sm.sale_line_id, 
                sol.order_id sale_id, 
                svl.product_id, 
                pdtp.id as product_template_id, 
                pdtp.categ_id as categ_id, 
                sm.product_uom as uom_id,
                svl.company_id as company_id,
                svl.quantity,
                sm.location_id as location_id, 
                sl.usage, 

                case when svl.quantity >0.00 then svl.quantity
                else 0.00
                end qty_in,
                case when svl.quantity >0.00 then svl.unit_cost
                else 0.00
                end cost_in,

                case when svl.value > 0.00 then svl.value
                when svl.value < 0.00 then 0.00
                end total_cost_in,

                case when svl.quantity <0.00 then abs(svl.quantity)
                else 0.00
                end qty_out,
                case when svl.quantity <0.00 then abs(svl.unit_cost)
                else 0.00
                end cost_out,

                case when svl.value < 0.00 then abs(svl.value)
                when svl.value > 0.00 then 0.00
                end total_cost_out,

                svl.create_date, 
                (select coalesce(sum(quantity),0.00) from stock_valuation_layer as svl2 where 
                  svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and svl2.id<svl.id order by svl.product_id,svl.create_date)
                  + coalesce(svl.quantity,0.00) as cantidad_actual,
                svl.unit_cost, 
                svl.value, 
                sm.date, 
                sm.date_expected, 
                sm.picking_type_id, 
                sm.warehouse_id, 
                sm.inventory_id

                from stock_valuation_layer as svl 
                left join stock_move sm on sm.id = svl.stock_move_id 
                
                left join stock_location sl on sl.id = sm.location_id 
                left join account_move am on am.id = svl.account_move_id 
                left join sale_order_line sol on sol.id = sm.sale_line_id
                left join sale_order so on so.id = sol.order_id 
                left join purchase_order_line pol on pol.id=sm.purchase_line_id 
                left join purchase_order po on po.id = pol.order_id
                left join product_product pdpd on pdpd.id = svl.product_id 
                left join product_template pdtp on pdtp.id = pdpd.product_tmpl_id
                where svl.company_id = %s 
                order by svl.product_id, svl.create_date, svl.id) as table_quantity

                left join 

                (select id,
                  (select coalesce(sum(value),0.00) from stock_valuation_layer as svl2 where 
                    svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and svl2.id<svl.id order by svl.product_id,svl.create_date)
                    + coalesce(svl.value,0.00) as valor_actual
                from stock_valuation_layer as svl 
                where svl.company_id = %s 
                order by product_id, create_date,id) table_value on

                table_value.id=table_quantity.id 
                where table_quantity.create_date <='%s' %s """ %(
                    self.company_id.id,
                    self.company_id.id,
                    self.fecha_final,
                    filter_clause or '') 

        return query

    #######################################################################################3
    def _get_datas(self):

        filter_clause = ""

        warehouses=tuple(self.dinamic_warehouse_ids.mapped('id'))
        len_warehouses = len(warehouses or '')
        if len_warehouses:
            filter_clause += " and table_quantity.warehouse_id %s %s" % (self.warehouse_option or 'in',
                str(warehouses) if len_warehouses!=1 else str(warehouses)[0:len(str(warehouses))-2] + ')')


        locations=tuple(self.dinamic_location_ids.mapped('id'))
        len_locations = len(locations or '')
        if len_locations:
            filter_clause += " and table_quantity.location_id %s %s" % (self.location_option or 'in',
                str(locations) if len_locations!=1 else str(locations)[0:len(str(locations))-2] + ')')


        products=tuple(self.dinamic_product_ids.mapped('id'))
        len_products = len(products or '')
        if len_products:
            filter_clause += " and table_quantity.product_id %s %s" % (self.product_option or 'in',
                str(products) if len_products!=1 else str(products)[0:len(str(products))-2] + ')')


        query = self.get_query_kardex(filter_clause)

        self.env.cr.execute(query)
        records = self.env.cr.dictfetchall()
        
        return records
    ###############################################################################



    def button_view_tree_report_kardex(self):

        self.ensure_one()

        self.valuation_report_line_ids.unlink()
        
        registro=[]

        lines = self._get_datas()
        
        for line in lines:
            registro.append((0,0,{
                'stock_valuation_layer_id':line['stock_valuation_layer_id'] or False,
                'move_id':line['move_id'] or False,
                'type':line['type'] or False,
                'account_move_id':line['account_move_id'] or False,
                'invoice_number':line['invoice_number'] or False,
                'origin':line['origin'] or False,
                'picking_id':line['picking_id'] or False,
                'purchase_line_id':line['purchase_line_id'] or False,
                'purchase_id':line['purchase_id'] or False,
                'sale_line_id':line['sale_line_id'] or False,
                'sale_id':line['sale_id'] or False,
                'product_id':line['product_id'] or False,
                'product_tmpl_id':line['product_tmpl_id'] or False,
                'categ_id':line['categ_id'] or False,
                'uom_id':line['uom_id'] or False,
                'company_id':line['company_id'] or False,
                'quantity':line['quantity'] or False,
                'location_id':line['location_id'] or False,
                'usage':line['usage'] or False,
                'qty_in':line['qty_in'] or False,
                'cost_in':line['cost_in'] or False,
                'total_cost_in':line['total_cost_in'] or False,
                'qty_out':line['qty_out'] or False,
                'cost_out':line['cost_out'] or False,
                'total_cost_out':line['total_cost_out'] or False,
                'qty':line['qty'] or False,
                'unit_cost':line['unit_cost'] or False,
                'value':line['value'] or False,
                'date':line['date'] or False,
                'date_expected':line['date_expected'] or False,
                'picking_type_id':line['picking_type_id'] or False,
                'warehouse_id':line['warehouse_id'] or False,
                'inventory_id':line['inventory_id'] or False,
                }))
        
        self.valuation_report_line_ids = registro
        

        view = self.env.ref('l10n_pe_account_stock_valuation_report.view_account_stock_valuation_report_tree')
        view_form = self.env.ref('l10n_pe_account_stock_valuation_report.view_account_stock_valuation_report_form')

        if self.valuation_report_line_ids:
            diccionario = {
                'name': 'Reporte Karde Valorizado',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'account.stock.valuation.report',
                'view_id': view.id,
                'views': [(view.id,'tree'),(view_form.id,'form')],
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', [i.id for i in self.valuation_report_line_ids] or [])],
                'context':{
                    'search_default_Product':1,
                    #'search_default_in_location':1,
                    }}
            return diccionario
