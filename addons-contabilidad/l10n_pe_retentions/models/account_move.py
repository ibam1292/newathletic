# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
from odoo.tools import float_is_zero, float_compare
from datetime import datetime, timedelta

import logging
_logger = logging.getLogger(__name__)

class AccountMove(models.Model):
	_inherit = 'account.move'

	retention_company_currency_id=fields.Many2one('res.currency',string="Moneda de la Compañia",
		default=lambda self: self.env['res.company']._company_default_get('account.invoice').currency_id)

	account_invoice_retention_ids = fields.Many2many('account.invoice.retention', 
		string="Registros de Retención de la Factura",readonly=True, states={'draft': [('readonly', False)]})

	is_retention=fields.Boolean(string="Documento sujeto a Retención",compute="_compute_retention",default=False)
	
	porcentaje_retencion=fields.Float(default=lambda self: float(self.env['ir.config_parameter'].sudo().get_param('porcentaje_retencion')),
		copy=False,string="% Retención ")
	
	monto_minimo_retencion = fields.Monetary(default=lambda self: float(self.env['ir.config_parameter'].sudo().get_param('monto_minimo_retencion')),
		copy=False,string="Monto Mínimo Retención")
	################################################################################################

	@api.depends('company_id','company_id.es_agente_retencion','company_id.currency_id','company_id.es_agente_percepcion',
		'company_id.es_buen_contribuyente','amount_total','porcentaje_retencion','monto_minimo_retencion',
		'partner_id.es_agente_retencion','amount_igv','invoice_date','currency_id')
	def _compute_retention(self):
		for rec in self:
			rec.is_retention = False
			if rec.amount_igv:
				if rec.partner_id.es_agente_retencion:
					if not(rec.company_id.es_agente_retencion or rec.company_id.es_agente_percepcion or rec.company_id.es_buen_contribuyente):
						
						aux_amount_in_company_currency = 0.00

						if rec.currency_id and rec.currency_id != rec.company_id.currency_id:
								aux_amount_in_company_currency=rec.currency_id._convert(rec.amount_total,
									rec.company_id.currency_id,rec.company_id,rec.invoice_date or fields.Date.context_today(rec))

						elif rec.currency_id:
								aux_amount_in_company_currency = rec.amount_total

						if aux_amount_in_company_currency > rec.monto_minimo_retencion:
							rec.is_retention=True
	###########################################################################################################

	def action_open_account_invoice_retention_ids(self):
		#self.ensure_one()
		#view = self.env.ref('l10n_pe_retentions.account_invoice_retention_view_tree')
		if self.account_invoice_retention_ids:

			return {
				'name': 'Registros de Retención',
				'view_mode': 'tree,form',
				'res_model': 'account.invoice.retention',
				'view_id': False,
				#'views': [(view.id, 'tree')],
				'type': 'ir.actions.act_window',
				"domain":[('id','in',
					list(self.account_invoice_retention_ids.mapped('id')) or [])],
				'context': {'company_id': self.company_id.id}}



class AccountMoveLine(models.Model):
	_inherit = "account.move.line"

	is_retention = fields.Char(string="Es Retencion", copy=False)
	invoice_retention_id = fields.Many2one('account.invoice.retention',string="Registro Retención",copy=False)