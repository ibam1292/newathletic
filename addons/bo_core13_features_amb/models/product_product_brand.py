# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError

class ProductProductBrand(models.Model):
	_name = 'product.product.brand'
	_description = "Marca de Producto"


	name = fields.Char(string="Nombre",default="")
	code = fields.Char(string="Código",default="")

	description = fields.Text(string="Descripción",default="")
	
	_sql_constraints = [
		('fiscal_month','unique(code)','El código de la marca ya existe, este debe ser único !'),
	]
