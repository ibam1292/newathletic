{
	'name': 'Marca en Productos y Cotizaciones',
	'version': "1.0.0",
	'author': 'Bigodoo - Christian',
	'website':'',
	'category':'',
	'depends':['stock','product','sale'],
	'description':'''
		Modulo de Marca en Productos y Cotizaciones.
			> Marca en Productos y Cotizaciones.
		''',
	'data':[
		'security/ir.model.access.xml',
		'views/bo_product_brand_view.xml',
		'views/product_template_view.xml',
		'views/sale_order_view.xml',
		'views/purchase_order_view.xml',
		'views/stock_picking_view.xml',
		'views/stock_quant_view.xml',
	],
	'installable': True,
    'auto_install': False,
}