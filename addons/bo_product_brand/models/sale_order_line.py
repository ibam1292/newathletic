from odoo import models, fields, api, _

class SaleOrderLine(models.Model):
    _inherit='sale.order.line'
		
    bo_brand_id = fields.Many2one('bo.product.brand', string="Marca", compute="_compute_bo_brand_id", store=True)

    @api.depends('product_id')
    def _compute_bo_brand_id(self):
        for rec in self:
            rec.bo_brand_id = rec.product_id.bo_brand_id if rec.product_id else False