from ..exceptions.exceptions import *
import requests
import re
import logging
from bs4 import BeautifulSoup
from ..utils.validations import *
_logger = logging.getLogger(__name__)


pattern_cpe_number = re.compile("^\d{8}$")

class GetStatusCDR(object):
    _endpoint = "https://e-factura.sunat.gob.pe/ol-it-wsconscpegem/billConsultService"

    def __init__(self) -> None:
        pass

    def login(self,username,password): 
        if not check_sunat_username(username) or not password:
            raise SunatLoginUsernameOrPasswordInvalid()

        self.username = username
        self.password = password

    def set_parameters(self,rucComprobante,tipoComprobante,serieComprobante,numeroComprobante):
        self.rucComprobante = rucComprobante
        self.tipoComprobante = tipoComprobante
        self.serieComprobante = serieComprobante
        self.numeroComprobante = numeroComprobante
    
    def fetch_cdr(self):
        status_code,response_text = self._request()
        result = self.extract_response_cdr(status_code,response_text)
        return result

    def _request(self):
        self._parameter_validation()
        try:
            return self._send()
        except Exception as e:
            raise e

    def _parameter_validation(self):
        if not check_ruc(self.rucComprobante):
            raise InvalidRUC()
        
        if self.tipoComprobante not in ["07","01"]:
            raise InvalidTipoCPE()
        
        if not check_serie_cpe(self.serieComprobante):
            raise InvalidSerieCPE()
        
        if not isinstance(self.numeroComprobante,int):
            raise ValueError("El número de comprobante debe ser de tipo entero.")
        
    def _send(self):
        headers = {"Content-Type": "text/xml; charset=utf-8"}
        data = self._body()
        response = requests.post(self._endpoint,data=data,headers=headers)
        return response.status_code,response.text
        

    def _body(self):
        body = '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
                                    xmlns:ser="http://service.sunat.gob.pe"
                                    xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                    <soapenv:Header>
                        <wsse:Security>
                            <wsse:UsernameToken>
                                <wsse:Username>{username}</wsse:Username>
                                <wsse:Password>{password}</wsse:Password>
                            </wsse:UsernameToken>
                        </wsse:Security>
                    </soapenv:Header>
                    <soapenv:Body>
                        <ser:getStatusCdr>
                            <rucComprobante>{rucComprobante}</rucComprobante>
                            <tipoComprobante>{tipoComprobante}</tipoComprobante>
                            <serieComprobante>{serieComprobante}</serieComprobante>
                            <numeroComprobante>{numeroComprobante}</numeroComprobante>
                        </ser:getStatusCdr>
                    </soapenv:Body>
                </soapenv:Envelope>'''
        
        return body.format(username=self.username or "", 
                            password=self.password or "", 
                            rucComprobante=self.rucComprobante or "",
                            tipoComprobante=self.tipoComprobante or "",
                            serieComprobante=self.serieComprobante or "",
                            numeroComprobante=int(self.numeroComprobante) or "")
    
    def extract_response_cdr(self,status_code,response_text):
        result = {}
        if status_code == 200:
            result = self._extract_response_200(response_text)
        else:
            result = self._extract_response_500(response_text)
        return result 
    
    def _extract_response_200(self,response_text):
        soup = BeautifulSoup(response_text, 'xml')
        statusCode = soup.find("statusCode").text if soup.find("statusCode") else ""
        content = soup.find("content").text if soup.find("content") else ""
        statusMessage = soup.find("statusMessage").text if soup.find("statusMessage") else ""
        return {"success":statusCode == "0004","statusCode":statusCode,"content":content,"statusMessage":statusMessage}
    
    def _extract_response_500(self,response_text):
        soup = BeautifulSoup(response_text, 'xml')
        statusCode = soup.find("faultcode").text if soup.find("faultcode") else ""
        content = ""
        statusMessage = soup.find("faultstring").text if soup.find("faultstring") else ""
        return {"success":False,"statusCode":statusCode,"content":content,"statusMessage":statusMessage}
    