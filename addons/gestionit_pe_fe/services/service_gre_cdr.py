import base64
import zipfile
import io
from ..exceptions.exceptions import ServiceGRECDRException
from bs4 import BeautifulSoup
import logging
_logger = logging.getLogger(__name__)

class ResponseGRECDR(object):
    def __init__(self,codRespuesta,error,arcCdr,indCdrGenerado) -> None:
        self.codRespuesta = codRespuesta
        self.error = error
        self.arcCdr = arcCdr
        self.indCdrGenerado = indCdrGenerado
    
    def get_document_description(self):
        content_xml = self._read_xml()
        soup = BeautifulSoup(content_xml, 'xml')
        return soup.find("cbc:DocumentDescription").text if soup.find("cbc:DocumentDescription") else ""
    
    def get_status(self):
        if self.codRespuesta == "0":
            return "A"
        elif self.codRespuesta == "98":
            return "E"
        elif self.codRespuesta == "99" and self.indCdrGenerado == "1":
            return "O"
        elif self.codRespuesta == "99":
            return "N"

    def resume_json(self):
        return {
            "codRespuesta":self.codRespuesta,
            "error":self.error,
            "arcCdr":self.arcCdr,
            "indCdrGenerado":self.indCdrGenerado
        }

    def _read_xml(self):
        if not self.arcCdr:
            return ""
        
        zip_data = base64.b64decode(self.arcCdr)
        zip_file = io.BytesIO(zip_data)
        with zipfile.ZipFile(zip_file, 'r') as zip_ref:
            xml_filename = zip_ref.namelist()[0]
            with zip_ref.open(xml_filename) as xml_file:
                content_xml = xml_file.read().decode('utf-8')
        return content_xml

class ServiceGRECDR(object):
    def __init__(self,client) -> None:
        self.client = client

    def get(self,ticket):
        return self._fetch(ticket)


    def _fetch(self,ticket):
        url = "https://api-cpe.sunat.gob.pe/v1/contribuyente/gem/comprobantes/envios/%s" % ticket
        response = self.client.make_request(method="GET",url=url)
        
        
        if response.status_code == 200:
            return self._extract_response_200(response)
        else:
            raise ServiceGRECDRException(self._response_message_error(response))
    
    def _extract_response_200(self,response):
        result = response.json()
        codRespuesta = result.get("codRespuesta","") 
        indCdrGenerado = result.get("indCdrGenerado","")
        arcCdr = result.get("arcCdr","") 
        error = result.get("error",{})
        error_message = ""
        if "error" in result:
            error_message = "%s %s" % (error.get("numError",""),error.get("desError",""))

        return ResponseGRECDR(codRespuesta,error_message,arcCdr,indCdrGenerado)

    def _response_message_error(self,response):
        result = response.json()
        if "cod" in result and "msg" in result:
            return "%s - %s" % (result.get("cod"),result.get("msg"))
        else:
            return response.text
    
