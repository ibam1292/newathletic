from ..exceptions.exceptions import *
from ..utils.validations import *
import urllib.parse
import requests
from datetime import timedelta,datetime
import json
ALLOWED_SCOPES = ["https://api-cpe.sunat.gob.pe",
                  "https://api.sunat.gob.pe/v1/contribuyente/contribuyentes",
                  "https://api-eeff.sunat.gob.pe"]

ALLOWED_GRANT_TYPES = ["Password","password","client_credentials"]

class SunatServicesApiClient(object):

    def client(self,username,password,client_id,client_secret,scope,grant_type):
        self.scope = scope
        self.grant_type = grant_type
        self.username = username
        self.password = password
        self.client_id = client_id
        self.client_secret = client_secret
        self.access_token = None
        self.expiration_date = datetime(2000, 1, 1)
        
    def get_token(self):
        try:
            if not self.access_token or self.expiration_date <= datetime.today():
                result = self._fetch_token()
                if "access_token" in result:
                    self.access_token = result.get("access_token",None)
                    self.expiration_date = datetime.today() + timedelta(hours=result.get("expires_in")/3600)
            return self.access_token
        except Exception as e:
            raise 
    
    def _fetch_token(self):
        self._validate()

        url = "https://api-seguridad.sunat.gob.pe/v1/clientessol/%s/oauth2/token/" % self.client_id

        payload_data = {
            "client_id":self.client_id,
            "client_secret":self.client_secret,
            "username":self.username,
            "password":self.password,
            "scope":self.scope,
            "grant_type":self.grant_type
        }

        payload = urllib.parse.urlencode(payload_data)
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        result = response.json()

        return result
    
    def _validate(self):
        if not check_sunat_username(self.username) or not self.password:
            raise SunatLoginUsernameOrPasswordInvalid()
        if not check_client_id(self.client_id) or not check_client_secret(self.client_secret):
            raise ValueError()
        if self.grant_type not in ALLOWED_GRANT_TYPES:
            raise ValueError("El grant_type no es un valor permitido. Valores permitidos %s" % ','.join(ALLOWED_GRANT_TYPES))
        if self.scope not in ALLOWED_SCOPES:
            raise ValueError("El scope no es un valor permitido. Valores permitidos %s" % ','.join(ALLOWED_SCOPES))

    def make_request(self,method,url,**args):
        access_token = self.get_token()
        headers = {
            "Authorization":"Bearer {}".format(access_token),
        }
        response = requests.request(method, url, data=json.dumps(args), headers=headers)

        return response
