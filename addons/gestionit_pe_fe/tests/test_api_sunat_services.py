
from odoo.tests.common import TransactionCase
from odoo.tests import tagged
from ..services.sunat_services_api_client import SunatServicesApiClient
from ..exceptions.exceptions import *



@tagged('post_install', '-at_install')
class TestSunatServicesAPIClient(TransactionCase):

    def setUp(self):
        super(TestSunatServicesAPIClient, self).setUp()
        #Las pruebas con datos reales, requiere que se establezcan los parametros _username, _password, _client_id y _client_secret
        self._username = ""
        self._password = ""
        self._client_id = ""
        self._client_secret = ""

    def test_get_token_1(self):
        list_credentials = [
            [["30608902211MODDATOS","aaaaAAzz998","lakshdfisdf54","nioahsdf"],SunatLoginUsernameOrPasswordInvalid],
            [["20608902211MODDATOS*","aaaaAAzz998 "," lakshdfisdf54 ","nioahsdf "],SunatLoginUsernameOrPasswordInvalid],
            [["20608902211MODDATOS","aaaaAAzz998 "," lakshdfisdf54 ",None],ValueError],
            [["20608902211MODDATOS","aaaaAAzz998"," lakshdfisdf54 ",None],ValueError],
            [["20608902211MODDATOS","**aaaaAAzz998"," lakshdfisdf54 ",None],ValueError],
        ]
        for record in list_credentials:
            creds = record[0]
            with self.assertRaises(record[1]):
                SunatServicesApiClientObject = SunatServicesApiClient()
                SunatServicesApiClientObject.client(username=creds[0],
                                                    password=creds[1],
                                                    client_id=creds[2],
                                                    client_secret=creds[3],
                                                    scope="https://api-cpe.sunat.gob.pe",
                                                    grant_type="password")
                SunatServicesApiClientObject.get_token()
    
    def test_get_token_2(self):
        if not self._username:
            return 
        SunatServicesApiClientObject = SunatServicesApiClient()
        SunatServicesApiClientObject.client(username=self._username,
                                            password=self._password,
                                            client_id=self._client_id,
                                            client_secret=self._client_secret,
                                            scope="https://api-cpe.sunat.gob.pe",
                                            grant_type="password")
        
        token = SunatServicesApiClientObject.get_token()
        self.assertTrue(isinstance(token,str))

    def test_make_request(self):
        if not self._username:
            return 
        SunatServicesApiClientObject = SunatServicesApiClient()
        SunatServicesApiClientObject.client(username=self._username,
                                            password=self._password,
                                            client_id=self._client_id,
                                            client_secret=self._client_secret,
                                            scope="https://api-cpe.sunat.gob.pe",
                                            grant_type="password")
        ticket = "c4d9daf4-abac-407d-88b6-a02f06f1cc8c"
        url = "https://api-cpe.sunat.gob.pe/v1/contribuyente/gem/comprobantes/envios/%s" % ticket
        response = SunatServicesApiClientObject.make_request(method="GET",url=url)
        result = response.json()
        self.assertEqual(result.get("codRespuesta"),"0")
        self.assertEqual(result.get("indCdrGenerado"),"1")