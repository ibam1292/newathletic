from odoo.tests.common import TransactionCase
from ..services.get_status_cdr import GetStatusCDR
from ..exceptions.exceptions import *
import unittest
import logging
from odoo.tests import tagged

_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestGestStatusCDR(TransactionCase):

    def setUp(self):
        super(TestGestStatusCDR, self).setUp()
        #Las pruebas con datos reales, requiere que se establezcan los parametros _username_real y _password_real
        self._username_real = ""
        self._password_real = ""

        self._username_fake = "20608902211MODDATOS"
        self._password_fake = "MODDATOS"

    def test_get_status_cdr_login_1(self):
        username = ""
        password = ""
        
        with self.assertRaises(SunatLoginUsernameOrPasswordInvalid):
            get_status_cdr_object = GetStatusCDR()
            get_status_cdr_object.login(username,password)

    def test_get_status_cdr_login_2(self):
        username = "ABC"
        password = "NUEVO"
        
        with self.assertRaises(SunatLoginUsernameOrPasswordInvalid):
            get_status_cdr_object = GetStatusCDR()
            get_status_cdr_object.login(username,password)

    def test_fetch_cdr_1(self):
        username = self._username_fake
        password = self._password_fake
        get_status_cdr_object = GetStatusCDR()
        get_status_cdr_object.login(username,password)
        
        test_data = [
            ["206089022AB","01","F001",7,InvalidRUC],
            ["30545474878","01","F001",7,InvalidRUC],
            ["00545474878","01","F001",7,InvalidRUC],
            [False,"01","F001",12,InvalidRUC],
            ["206089022AB","01","F001",12,InvalidRUC],
            ["20608902211","11","F001",7,InvalidTipoCPE],
            ["20608902211","12","F001",7,InvalidTipoCPE],
            ["20608902211","07","X001",7,InvalidSerieCPE],
            ["20608902211","07","NV",7,InvalidSerieCPE],
            ["20608902211","07","F001","43",ValueError],
            ["20608902211","07","F001",13.5,ValueError],
        ]

        for line in test_data:
            with self.assertRaises(line[4]):
                get_status_cdr_object.set_parameters(line[0],line[1],line[2],line[3])
                get_status_cdr_object.fetch_cdr()
            
    def test_fetch_cdr_2(self):
        username = self._username_fake
        password = self._password_fake
        get_status_cdr_object = GetStatusCDR()
        get_status_cdr_object.login(username,password)
        
        test_data = [
            [["20608902211","01","FA11",223],{"statusCode":"ns0:0103","statusMessage":"El Usuario ingresado no existe"}],
        ]

        for line in test_data:
            cpe = line[0]
            get_status_cdr_object.set_parameters(cpe[0],cpe[1],cpe[2],cpe[3])            
            result_cdr = get_status_cdr_object.fetch_cdr()
            self.assertEqual(result_cdr.get("statusCode"), line[1].get("statusCode"), msg=result_cdr.get("statusMessage"))
            
    def test_fetch_cdr_4(self):
        if not self._username_real:
            return
        
        username = self._username_real
        password = self._password_fake
        get_status_cdr_object = GetStatusCDR()
        get_status_cdr_object.login(username,password)
        
        test_data = [
            [["20608902211","01","FA11",224],{"statusCode":"ns0:0104","statusMessage":"La Clave ingresada es incorrecta"}]
        ]

        for line in test_data:
            cpe = line[0]
            get_status_cdr_object.set_parameters(cpe[0],cpe[1],cpe[2],cpe[3])            
            result_cdr = get_status_cdr_object.fetch_cdr()
            self.assertEqual(result_cdr.get("statusCode"), line[1].get("statusCode"), msg=result_cdr.get("statusMessage"))
            

    def test_fetch_cdr_5(self):
        if not self._username_real:
            return

        username = self._username_real
        password = self._password_real
        get_status_cdr_object = GetStatusCDR()
        get_status_cdr_object.login(username,password)
        
        test_data = [
            [["20608902211","01","FA11",223],{"statusCode":"0004","statusMessage":"La constancia existe"}],
            [["20608902211","01","FA11",171],{"statusCode":"0004","statusMessage":"La constancia existe"}],
            [["20608902211","01","FA12",224],{"statusCode":"0127","statusMessage":"El ticket no existe"}],
        ]

        for line in test_data:
            cpe = line[0]
            get_status_cdr_object.set_parameters(cpe[0],cpe[1],cpe[2],cpe[3])            
            result_cdr = get_status_cdr_object.fetch_cdr()
            self.assertEqual(result_cdr.get("statusCode"), line[1].get("statusCode"), msg=result_cdr.get("statusMessage"))
