from odoo.tests.common import TransactionCase
from ..services.service_gre_cdr import ServiceGRECDR
from ..exceptions.exceptions import *
from .test_api_sunat_services import TestSunatServicesAPIClient
from odoo.tests import tagged
from ..services.sunat_services_api_client import SunatServicesApiClient
from ..services.service_gre_cdr import ServiceGRECDR
from ..exceptions.exceptions import *


@tagged('post_install', '-at_install')
class TestGetGRECDR(TestSunatServicesAPIClient):
    def setUp(self):
        super(TestGetGRECDR, self).setUp()
        #Establezca un _ticket_real y un ticket_fake para realizar pruebas unitarias
        self._ticket_real = ""
        self._ticket_fake = "c4d9daf4-xxxx-aaaa-88b6-a02f06f1cc8c"

    def test_service_gre_cdr_envio_ok(self):
        if not self._ticket_real:
            return 
        
        SunatServicesApiClientObject = SunatServicesApiClient()
        SunatServicesApiClientObject.client(username=self._username,
                                            password=self._password,
                                            client_id=self._client_id,
                                            client_secret=self._client_secret,
                                            scope="https://api-cpe.sunat.gob.pe",
                                            grant_type="password")
        ServiceGRECDRObject = ServiceGRECDR(SunatServicesApiClientObject)
        gre_cdr_object = ServiceGRECDRObject.get(self._ticket_real)
        
        self.assertTrue(gre_cdr_object.codRespuesta,"0")
        self.assertTrue(gre_cdr_object.indCdrGenerado,"1")
        
    def test_get_gre_cdr_none(self):
        if not self._ticket_fake:
            return 
        SunatServicesApiClientObject = SunatServicesApiClient()
        SunatServicesApiClientObject.client(username=self._username,
                                            password=self._password,
                                            client_id=self._client_id,
                                            client_secret=self._client_secret,
                                            scope="https://api-cpe.sunat.gob.pe",
                                            grant_type="password")
        #Ticket no existe
        ServiceGRECDRObject = ServiceGRECDR(SunatServicesApiClientObject)
        with self.assertRaises(ServiceGRECDRException):
            ServiceGRECDRObject.get(self._ticket_fake)



    def test_get_gre_cdr_none(self):
        if not self._ticket_real:
            return 
        SunatServicesApiClientObject = SunatServicesApiClient()
        SunatServicesApiClientObject.client(username=self._username,
                                            password=self._password,
                                            client_id=self._client_id,
                                            client_secret=self._client_secret,
                                            scope="https://api-cpe.sunat.gob.pe",
                                            grant_type="password")
        ServiceGRECDRObject = ServiceGRECDR(SunatServicesApiClientObject)
        ResponseGRECDRObject = ServiceGRECDRObject.get(self._ticket_real)
        document_description = ResponseGRECDRObject.get_document_description()

        self.assertTrue("https://e-factura.sunat.gob.pe/v1/contribuyente/gre/comprobantes/descargaqr" in document_description)

        
