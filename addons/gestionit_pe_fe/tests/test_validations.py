from odoo.tests.common import TransactionCase
from odoo.tests import tagged
from ..utils.validations import *

@tagged("post_install","-at_install")
class TestValidations(TransactionCase):

    def setUp(self):
        super(TestValidations,self).setUp()

    def tests_check_ruc(self):
        self.assertTrue(check_ruc("20608902211"))
        self.assertTrue(check_ruc("10763104201"))
        self.assertFalse(check_ruc("20608902213"))
        self.assertFalse(check_ruc("10763104202"))
        self.assertFalse(check_ruc("30763104202"))
        self.assertFalse(check_ruc("30763104202 "))
        self.assertFalse(check_ruc("3076310420 "))
        self.assertFalse(check_ruc(3076310420))


    def tests_check_dni(self):
        self.assertTrue(check_dni("00544988"))
        self.assertTrue(check_dni("76311111"))
        self.assertFalse(check_dni("76311111 "))
        
        self.assertFalse(check_dni("002584"))
        self.assertFalse(check_dni("ABC1235"))
        self.assertFalse(check_dni(None))


    def tests_check_serie_cpe(self):
        self.assertTrue(check_serie_cpe("F002"))
        self.assertTrue(check_serie_cpe("FAAA"))
        self.assertFalse(check_serie_cpe("FAAAA"))
        self.assertFalse(check_serie_cpe("F BC"))
        self.assertFalse(check_serie_cpe("F_12"))
        self.assertFalse(check_serie_cpe("GHIJ"))
        self.assertFalse(check_serie_cpe(None))
        
        
    def tests_check_sunat_username(self):
        self.assertTrue(check_sunat_username("20608902211MODDATOS"))
        self.assertTrue(check_sunat_username("20608902211ABC1234"))
        self.assertFalse(check_sunat_username("20608902213bbbbbbbX "))
        self.assertFalse(check_sunat_username("20608902213 bbbbbbbX"))
        self.assertFalse(check_sunat_username(None))

    def test_check_client_id(self):
        self.assertTrue(check_client_id("asndfahsdfasd-asdf2351-asdfh923"))
        self.assertFalse(check_client_id("asd*239hf asdf23"))
        self.assertFalse(check_client_id(None))

    def test_check_client_secret(self):
        self.assertTrue(check_client_secret("asndfahsdfasd-asdf2351-asdfh923"))
        self.assertFalse(check_client_secret(" asndfahsdfasd-asdf2351-asdfh923"))
        self.assertFalse(check_client_secret("9SjwSbwyg9o1JVe3LpPAng== "))
        self.assertFalse(check_client_secret("9SjwSbw yg9o1JVe3LpPAng=="))
        self.assertFalse(check_client_secret(None))