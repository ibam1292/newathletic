import re
pattern_ruc = re.compile("^[12]\d{10}$")
pattern_dni = re.compile("^\d{8}$")
pattern_username = re.compile("^[12]\d{10}[a-zA-Z0-9]{3,10}$")
pattern_serie = re.compile("^[FB][A-Z0-9]{3}$")
pattern_cpe_number = re.compile("^\d{8}$")

pattern_client_id = re.compile("^[a-zA-Z0-9_-]+$")
pattern_client_secret = re.compile("^[a-zA-Z0-9]+$")


def check_ruc(ruc):
    if not isinstance(ruc,str):
        return False

    if not pattern_ruc.match(ruc) or len(ruc) != 11:
        return False
    
    dig_check = 11 - (sum([int('5432765432'[f]) * int(ruc[f]) for f in range(0, 10)]) % 11)
    if dig_check == 10:
        dig_check = 0
    elif dig_check == 11:
        dig_check = 1
    return int(ruc[10]) == dig_check

def check_dni(dni):
    if not isinstance(dni, str):
        return False
    
    if pattern_dni.match(dni) and len(dni) == 8:
        return True
    return False


def check_serie_cpe(serie):
    if not isinstance(serie,str):
        return False

    if pattern_serie.match(serie):
        return True
    return False

def check_sunat_username(username):
    if not isinstance(username,str):
        return False

    if not pattern_username.match(username) or " " in username:
        return False
    
    return True


def check_client_id(client_id):
    if not isinstance(client_id,str):
        return False
    
    if " " in client_id:
        return False
    
    return True

def check_client_secret(client_secret):
    if not isinstance(client_secret,str):
        return False
    
    if " " in client_secret:
        return False
    
    return True
    