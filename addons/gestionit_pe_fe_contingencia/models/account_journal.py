# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import re
import logging
import requests
import json
_logger = logging.getLogger(__name__)

class AccountJournal(models.Model):
    _inherit = 'account.journal'

    is_contingencia = fields.Boolean(string="Es Contingencia")

    @api.constrains("code","is_contingencia")
    def constrains_code(self):
        for record in self:
            if not record.is_contingencia:
                super(AccountJournal,self).constrains_code()