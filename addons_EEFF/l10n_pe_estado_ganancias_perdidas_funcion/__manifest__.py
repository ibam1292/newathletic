{
	'name': 'ESTADO DE GANANCIAS Y PERDIDAS POR FUNCIÓN',
	'version': "1.0.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends':['base','account','unique_library_accounting_queries',
		'general_balance_report','report_formats'],
	'description':'''
		Modulo de Reporte de Estado de Ganancias y Pérdidas por Función.
		''',
	'data':[
		'security/ir.model.access.csv',
		'views/template_ganancias_perdidas_report_view.xml',
		'views/template_ganancias_perdidas_rubro_view.xml',
		'views/template_ganancias_perdidas_sub_rubro_view.xml',
		'views/estado_ganancias_perdidas_funcion_report_view.xml',
		'views/estado_ganancias_perdidas_funcion_report_line_view.xml',
	],
	'installable': True,
    'auto_install': False,
}