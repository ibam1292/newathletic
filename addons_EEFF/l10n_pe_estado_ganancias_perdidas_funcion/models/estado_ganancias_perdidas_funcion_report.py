import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from odoo.exceptions import UserError , ValidationError
from odoo.addons import unique_library_accounting_queries as unique_queries
import logging
from itertools import *
_logger=logging.getLogger(__name__)

options=[
	('in','esta en'),
	('not in','no esta en')
	]


DICT_MESES = {
	1:'Enero',
	2:'Febrero',
	3:'Marzo',
	4:'Abril',
	5:'Mayo',
	6:'Junio',
	7:'Julio',
	8:'Agosto',
	9:'Septiembre',
	10:'Octubre',
	11:'Noviembre',
	12:'Diciembre'
		}


meses=[
	('01','Enero'),
	('02','Febrero'),
	('03','Marzo'),
	('04','Abril'),
	('05','Mayo'),
	('06','Junio'),
	('07','Julio'),
	('08','Agosto'),
	('09','Septiembre'),
	('10','Octubre'),
	('11','Noviembre'),
	('12','Diciembre')]


class EstadoGananciasPerdidasFuncionReport(models.Model):

	_name='estado.ganancias.perdidas.funcion.report'
	_description = "Modulo de Reporte Estado Ganancias-Pérdidas por Función"


	state = fields.Selection(selection=[('draft','Borrador'),('generated','Generado')],
		readonly=True, states={'draft': [('readonly', False)]},
		string="Estado", default="draft")

	company_id = fields.Many2one('res.company',
		string="Compañia", 
		default=lambda self: self.env['res.company']._company_default_get('account.invoice'),
		domain = lambda self: [('id', 'in',[i.id for i in self.env['res.users'].browse(self.env.user.id).company_ids] )],
		readonly=True)

	report_line_ids = fields.One2many('estado.ganancias.perdidas.funcion.report.line',
		'ganancias_perdidas_funcion_report_id',
		string="Rubros del Estado de Ganancias-Pérdidas por Función",
		readonly=True, states={'draft': [('readonly', False)]})

	######################################################
	name = fields.Char(string="Nombre")
	observations = fields.Char(string="Observaciones")
	########################################################################

	report_type = fields.Selection(selection=[('cum','Acumulado'),('month','Mensualizado')],string="Tipo de Reporte",required=True)

	fiscal_year = fields.Selection(selection=[(str(num), str(num)) for num in reversed(range(2000, (datetime.now().year) + 1 ))],
		string="Año fiscal")
	mes_inicio = fields.Selection(selection=meses,string="Mes Inicial")
	mes_final = fields.Selection(selection=meses,string="Mes Final")

	fecha_inicio=fields.Date(string="Fecha Inicio")
	fecha_final=fields.Date(string="Fecha Final")

	########################################################################

	#### FILTROS DINAMICOS !!
	partner_ids = fields.Many2many('res.partner',string="Socios")
	partner_option=fields.Selection(selection=options , string="")

	account_ids = fields.Many2many('account.account',string='Cuentas')
	account_option=fields.Selection(selection=options , string="")

	journal_ids = fields.Many2many('account.journal',string="Diarios")
	journal_option=fields.Selection(selection=options , string="")

	move_ids = fields.Many2many('account.move',string='Asientos Contables')
	move_option=fields.Selection(selection=options , string="")


	template_ganancias_perdidas_report_id = fields.Many2one('template.ganancias.perdidas.report',
		string="Plantilla Estado de Ganancias-Pérdidas por Función",
		default=lambda self:self.env['template.ganancias.perdidas.report'].search([],limit=1))

	######################################################
	print_format = fields.Selection(selection='_get_print_format',string='Formato Impresión',default='xlsx')
	#######################################################
	
	def name_get(self):
		result = []
		for rec in self:
			result.append((rec.id,"%s-%s"%(self._convert_object_date(rec.fecha_inicio),self._convert_object_date(rec.fecha_final)) or 'New'))
		return result

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		recs = self.search([('fecha_inicio', operator, name),('fecha_final', operator, name)] + args, limit=limit)
		return recs.name_get()


	##########################################################
	@api.model
	def _get_print_format(self):
		option = [
			('xlsx','xlsx')
		]
		return option


	def document_print(self):
		output = BytesIO()
		output = self._init_buffer(output)
		output.seek(0)
		return output.read()


	def action_print(self):
		if (self.print_format) :

			if self.print_format in ['xlsx']:
				return {
					'type': 'ir.actions.act_url',
					'url': 'reports/format/{}/{}/{}'.format(self._name, self.print_format, self.id),
					'target': 'new'}
		else:
			raise UserError(_('NO SE PUEDE IMPRIMIR !\nEl campo Formato de Impresión es obligatorio, por favor llene dicho campo.'))

	
	#######################################################################

	def button_view_pivot_year_month(self):
		self.ensure_one()
		view = self.env.ref('l10n_pe_estado_ganancias_perdidas_funcion.view_estado_ganacias_perdidas_funcion_report_line_year_month_pivot')

		if self.report_line_ids:
			diccionario = {
				'view_type': 'pivot',
				'view_mode': 'pivot,tree,form',
				'res_model': 'estado.ganancias.perdidas.funcion.report.line',
				'view_id': view.id,
				'views': [(view.id,'pivot')],
				'type': 'ir.actions.act_window',
				'domain': [('id', 'in', [i.id for i in self.report_line_ids] or [])],
				'context':{
					'search_default_filter_name_rubro':1,
					'search_default_filter_name_sub_rubro':1,
					'search_default_filter_name_account_id':1,
					}
			}

			if self.report_type == 'month':
				diccionario['name'] = 'Estado de Ganancias-Pérdidas por Función de %s/%s a %s/%s'%(
					DICT_MESES[int(self.mes_inicio)],self.fiscal_year,DICT_MESES[int(self.mes_final)],self.fiscal_year)

			return diccionario


	def button_view_pivot_cum(self):
		self.ensure_one()
		view = self.env.ref('l10n_pe_estado_ganancias_perdidas_funcion.view_estado_ganacias_perdidas_funcion_report_line_cum_pivot')
		
		if self.report_line_ids:
			diccionario = {
				'view_type': 'pivot',
				'view_mode': 'pivot,tree,form',
				'res_model': 'estado.ganancias.perdidas.funcion.report.line',
				'view_id': view.id,
				'views': [(view.id,'pivot')],
				'type': 'ir.actions.act_window',
				'domain': [('id', 'in', [i.id for i in self.report_line_ids] or [])],
				'context':{
					'search_default_filter_name_rubro':1,
					'search_default_filter_name_sub_rubro':1,
					'search_default_filter_name_account_id':1,
					}
			}

			if self.report_type == 'cum':
				diccionario['name'] = 'Estado de Ganancias-Pérdidas por Función de %s a %s'%(
					self.fecha_inicio.strftime("%d-%m-%Y"),self.fecha_final.strftime("%d-%m-%Y"))

			return diccionario


	###########################################################################		


	
	def query_balance_of_sums_and_balances(self,str_account_ids,fecha_movimiento_debe,fecha_movimiento_haber):

		filter_clause = ""
		partners=tuple(self.partner_ids.mapped('id'))
		len_partners = len(partners or '')
		if len_partners:
			filter_clause += " and aml.partner_id %s %s" % ('in' or self.partner_option , str(partners) if len_partners!=1 else str(partners)[0:len(str(partners))-2] + ')')

		journals = tuple(self.journal_ids.mapped('id'))
		len_journals = len(journals or '')
		if len(self.journal_ids):
			filter_clause += " and aml.journal_id %s %s " % ('in' or self.journal_option , str(journals) if len_journals!=1 else str(journals)[0:len(str(journals))-2] + ')')

		moves = tuple(self.move_ids.mapped('id'))
		len_moves = len(moves or '')
		if len(moves):
			filter_clause += " and aml.move_id %s %s " % ('in' or self.move_option , str(moves) if len_moves!=1 else str(moves)[0:len(str(moves))-2] + ')')


		accounts = tuple(self.account_ids.mapped('id'))
		len_accounts = len(accounts or '')
		if len(accounts):
			filter_clause += " and aml.account_id %s %s " % ('in' or self.account_option , str(accounts) if len_accounts!=1 else str(accounts)[0:len(str(accounts))-2] + ')')

		query = unique_queries.query_account_amount_balances_group_account(str_account_ids,fecha_movimiento_debe,fecha_movimiento_haber,filter_clause)

		self.env.cr.execute(query)
		records = self.env.cr.dictfetchall()
		return records




	def file_name(self, file_format):
		
		file_name = "Estado_Ganancias_Perdidas_Funcion_%s_%s_%s.%s" % (self.company_id.vat,
			self.fecha_inicio.strftime('%d_%m_%Y'),self.fecha_final.strftime('%d_%m_%Y'),file_format)
		
		return file_name



	
	def generar_libro(self):
		self.state = 'generated'
		self.report_line_ids.unlink()
		registro=[]

		#####################################################################

		rubros_ganancias_perdidas = self.template_ganancias_perdidas_report_id.ganancias_perdidas_rubro_ids
		
		TOTAL_ACUMULADO = 0.00

		### CONTADOR DE ITEM ###
		nro_item = 1
		nro_sub_item = 1

		if self.report_type == 'cum':

			for rubro in rubros_ganancias_perdidas:

				if rubro.type_rubro == 'rubro' and rubro.operation_type in ['+','-']:

					if rubro.calculation_type=='accounts':
						
						if rubro.account_ids:
							rubro_cuentas = self.query_balance_of_sums_and_balances(rubro.account_ids.mapped('id'),
								self.fecha_inicio.strftime('%Y-%m-%d'),self.fecha_final.strftime('%Y-%m-%d'))
							
							for rc in rubro_cuentas:
								if rubro.operation_type == '+':					
									registro.append((0,0,{
										'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'account_id':rc['account_id'] or False,
										'saldo_rubro_contable':rc['balance'] or 0.00,
										
										}))

									nro_item += 1
									TOTAL_ACUMULADO += rc['balance'] or 0.00

								elif rubro.operation_type == '-':
									registro.append((0,0,{
										'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'account_id':rc['account_id'] or False,
										'saldo_rubro_contable':(-1.00)*(rc['balance'] or 0.00),
										
										}))

									nro_item += 1
									TOTAL_ACUMULADO -= rc['balance'] or 0.00


						elif rubro.calculation_type=='manual':

							if rubro.operation_type == '+':		

								registro.append((0,0,{
									'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
									'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
									'account_id':rc['account_id'] or False,
									'saldo_rubro_contable':rubro.movements_period or 0.00,
									
									}))

								nro_item += 1
								TOTAL_ACUMULADO += rubro.movements_period or 0.00

							elif rubro.operation_type == '-':		

								registro.append((0,0,{
									'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
									'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
									'account_id':rc['account_id'] or False,
									'saldo_rubro_contable':(-1.00)*(rubro.movements_period or 0.00),
									
									}))

								nro_item += 1
								TOTAL_ACUMULADO -= rubro.movements_period or 0.00

				elif rubro.type_rubro == 'rubro' and rubro.operation_type in ['=']:

					registro.append((0,0,{
						'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
						'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
						'account_id': False,
						'saldo_rubro_contable':TOTAL_ACUMULADO or 0.00,
						
						}))

					nro_item += 1

				elif rubro.type_rubro == 'rubro' and not rubro.operation_type:

					registro.append((0,0,{
						'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
						'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
						'account_id': False,
						'saldo_rubro_contable':0.00,
						
						}))

					nro_item += 1

				##############################################################################
				elif rubro.type_rubro == 'rubro_padre':

					SUB_TOTAL_ACUMULADO = 0.00

					for sub_rubro in rubro.sub_rubro_line_ids:

						if sub_rubro.operation_type in ['+','-']:

							if sub_rubro.calculation_type=='accounts':
								
								if sub_rubro.account_ids:

									rubro_cuentas = self.query_balance_of_sums_and_balances(sub_rubro.account_ids.mapped('id'),
										self.fecha_inicio.strftime('%Y-%m-%d'),self.fecha_final.strftime('%Y-%m-%d'))
									
									for rc in rubro_cuentas:

										if sub_rubro.operation_type == '+':

											registro.append((0,0,{
												'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
												'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
												'account_id':rc['account_id'] or False,
												'saldo_rubro_contable':rc['balance'] or 0.00,
												
												}))

											nro_item += 1
											nro_sub_item += 1
											SUB_TOTAL_ACUMULADO += rc['balance'] or 0.00
											TOTAL_ACUMULADO += rc['balance'] or 0.00


										elif sub_rubro.operation_type == '-':
											registro.append((0,0,{
												'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
												'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
												'account_id':rc['account_id'] or False,
												'saldo_rubro_contable':(-1.00)*(rc['balance'] or 0.00),
												
												}))

											nro_item += 1
											SUB_TOTAL_ACUMULADO -= rc['balance'] or 0.00
											TOTAL_ACUMULADO -= rc['balance'] or 0.00


							elif sub_rubro.calculation_type=='manual':

								if sub_rubro.operation_type == '+':		

									registro.append((0,0,{
										'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
										'account_id':rc['account_id'] or False,
										'saldo_rubro_contable':sub_rubro.movements_period or 0.00,
										
										}))

									nro_item += 1
									SUB_TOTAL_ACUMULADO += sub_rubro.movements_period or 0.00
									TOTAL_ACUMULADO += sub_rubro.movements_period or 0.00

								elif sub_rubro.operation_type == '-':		

									registro.append((0,0,{
										'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
										'account_id':rc['account_id'] or False,
										'saldo_rubro_contable':(-1.00)*(sub_rubro.movements_period or 0.00),
										
										}))

									nro_item += 1
									SUB_TOTAL_ACUMULADO -= sub_rubro.movements_period or 0.00
									TOTAL_ACUMULADO -= sub_rubro.movements_period or 0.00

						elif sub_rubro.operation_type in ['=']:

							registro.append((0,0,{
								'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
								'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
								'account_id': False,
								'saldo_rubro_contable':SUB_TOTAL_ACUMULADO or 0.00,
								
								}))

							nro_item += 1

		#################################################################

		elif self.report_type =='month':

			for mes in range(int(self.mes_inicio),int(self.mes_final)):

				TOTAL_ACUMULADO = 0.00

				fecha_inicio = "%s-%s-%s"%(self.fiscal_year,'{:02}'.format(mes),'01')

				fecha_final = "%s-%s-%s"%(self.fiscal_year,'{:02}'.format(mes),
					str(calendar.monthrange(int(self.fiscal_year),mes)[1]))

				for rubro in rubros_ganancias_perdidas:

					if rubro.type_rubro == 'rubro' and rubro.operation_type in ['+','-']:

						if rubro.calculation_type=='accounts':
							
							if rubro.account_ids:

								rubro_cuentas = self.query_balance_of_sums_and_balances(rubro.account_ids.mapped('id'),fecha_inicio,fecha_final)
								
								for rc in rubro_cuentas:
									if rubro.operation_type == '+':					
										registro.append((0,0,{
											'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
											'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
											'account_id':rc['account_id'] or False,
											'year':self.fiscal_year or '',
											'month':DICT_MESES[mes] or '',
											'saldo_rubro_contable':rc['balance'] or 0.00,
											
											}))

										nro_item += 1
										TOTAL_ACUMULADO += rc['balance'] or 0.00

									elif rubro.operation_type == '-':
										registro.append((0,0,{
											'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
											'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
											'account_id':rc['account_id'] or False,
											'year':self.fiscal_year or '',
											'month':DICT_MESES[mes] or '',
											'saldo_rubro_contable':(-1.00)*(rc['balance'] or 0.00),
											
											}))

										nro_item += 1
										TOTAL_ACUMULADO -= rc['balance'] or 0.00


							elif rubro.calculation_type=='manual':

								if rubro.operation_type == '+':		

									registro.append((0,0,{
										'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'account_id':rc['account_id'] or False,
										'year':self.fiscal_year or '',
										'month':DICT_MESES[mes] or '',
										'saldo_rubro_contable':rubro.movements_period or 0.00,
										
										}))

									nro_item += 1
									TOTAL_ACUMULADO += rubro.movements_period or 0.00

								elif rubro.operation_type == '-':		

									registro.append((0,0,{
										'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
										'account_id':rc['account_id'] or False,
										'year':self.fiscal_year or '',
										'month':DICT_MESES[mes] or '',
										'saldo_rubro_contable':(-1.00)*(rubro.movements_period or 0.00),
										
										}))

									nro_item += 1
									TOTAL_ACUMULADO -= rubro.movements_period or 0.00

					elif rubro.type_rubro == 'rubro' and rubro.operation_type in ['=']:

						registro.append((0,0,{
							'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
							'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
							'account_id': False,
							'year':self.fiscal_year or '',
							'month':DICT_MESES[mes] or '',
							'saldo_rubro_contable':TOTAL_ACUMULADO or 0.00,
							
							}))

						nro_item += 1

					elif rubro.type_rubro == 'rubro' and not rubro.operation_type:

						registro.append((0,0,{
							'name_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
							'name_sub_rubro': "%s %s"%(rubro.nro_rubro, rubro.name or False),
							'account_id': False,
							'year':self.fiscal_year or '',
							'month':DICT_MESES[mes] or '',
							'saldo_rubro_contable':0.00,
							
							}))

						nro_item += 1

					##############################################################################
					elif rubro.type_rubro == 'rubro_padre':

						SUB_TOTAL_ACUMULADO = 0.00

						for sub_rubro in rubro.sub_rubro_line_ids:

							if sub_rubro.operation_type in ['+','-']:

								if sub_rubro.calculation_type=='accounts':
									
									if sub_rubro.account_ids:

										rubro_cuentas = self.query_balance_of_sums_and_balances(sub_rubro.account_ids.mapped('id'),
											fecha_inicio,fecha_final)
										
										for rc in rubro_cuentas:

											if sub_rubro.operation_type == '+':

												registro.append((0,0,{
													'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
													'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
													'account_id':rc['account_id'] or False,
													'year':self.fiscal_year or '',
													'month':DICT_MESES[mes] or '',
													'saldo_rubro_contable':rc['balance'] or 0.00,
													
													}))

												nro_item += 1
												SUB_TOTAL_ACUMULADO += rc['balance'] or 0.00
												TOTAL_ACUMULADO += rc['balance'] or 0.00


											elif sub_rubro.operation_type == '-':
												registro.append((0,0,{
													'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
													'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
													'account_id':rc['account_id'] or False,
													'year':self.fiscal_year or '',
													'month':DICT_MESES[mes] or '',
													'saldo_rubro_contable':(-1.00)*(rc['balance'] or 0.00),
													
													}))

												nro_item += 1
												SUB_TOTAL_ACUMULADO -= rc['balance'] or 0.00
												TOTAL_ACUMULADO -= rc['balance'] or 0.00


								elif sub_rubro.calculation_type=='manual':

									if sub_rubro.operation_type == '+':		

										registro.append((0,0,{
											'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
											'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
											'account_id':rc['account_id'] or False,
											'year':self.fiscal_year or '',
											'month':DICT_MESES[mes] or '',
											'saldo_rubro_contable':sub_rubro.movements_period or 0.00,
											
											}))

										nro_item += 1
										SUB_TOTAL_ACUMULADO += sub_rubro.movements_period or 0.00
										TOTAL_ACUMULADO += sub_rubro.movements_period or 0.00

									elif sub_rubro.operation_type == '-':		

										registro.append((0,0,{
											'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
											'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
											'account_id':rc['account_id'] or False,
											'year':self.fiscal_year or '',
											'month':DICT_MESES[mes] or '',
											'saldo_rubro_contable':(-1.00)*(sub_rubro.movements_period or 0.00),
											
											}))

										nro_item += 1
										SUB_TOTAL_ACUMULADO -= sub_rubro.movements_period or 0.00
										TOTAL_ACUMULADO -= sub_rubro.movements_period or 0.00

							elif sub_rubro.operation_type in ['=']:

								registro.append((0,0,{
									'name_rubro': "%s %s"%(sub_rubro.nro_rubro, rubro.name or False),
									'name_sub_rubro': "%s.%s %s"%(sub_rubro.nro_rubro,sub_rubro.nro_sub_rubro,sub_rubro.name or False),
									'account_id': False,
									'year':self.fiscal_year or '',
									'month':DICT_MESES[mes] or '',
									'saldo_rubro_contable':SUB_TOTAL_ACUMULADO or 0.00,
									
									}))

								nro_item += 1

	

		self.report_line_ids = registro



	
	def _init_buffer(self, output):
		# output parametro de buffer que ingresa vacio
		if self.print_format == 'xlsx':
			self._generate_xlsx(output)

		return output

	def _convert_object_date(self, date):
		# parametro date que retorna un valor vacio o el foramto 01/01/2100 dia/mes/año
		if date:
			return date.strftime('%d/%m/%Y')
		else:
			return ''


	def is_menor(self,a,b):
		return a<b