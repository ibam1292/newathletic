import pytz
import calendar
import base64
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime, timedelta

import logging
_logger=logging.getLogger(__name__)

class EstadoGananciasPerdidasFuncionReportLine(models.Model):
	_name = 'estado.ganancias.perdidas.funcion.report.line'
	_order = 'nro_item'

	ganancias_perdidas_funcion_report_id = fields.Many2one('estado.ganancias.perdidas.funcion.report',
		string="Reporte de Estado Ganancias-Pérdidas por Función",ondelete="cascade",readonly=True)

	name_rubro = fields.Char(string="Rubro",readonly=True)
	name_sub_rubro = fields.Char(string="Sub-Rubro",readonly=True)

	account_id = fields.Many2one("account.account",
		string="Cuenta Asociada al Rubro",readonly=True)
	saldo_rubro_contable = fields.Float(string="Saldo Rubro",readonly=True)

	######################################################
	nro_item = fields.Integer(string="#",readonly=True)

	year = fields.Char(string="Año",readonly=True)
	month = fields.Char(string="Mes",readonly=True)

	######################################################

	def _convert_object_date(self, date):
		if date:
			return date.strftime('%d/%m/%Y')
		else:
			return ''