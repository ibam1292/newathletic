# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError
from datetime import datetime, timedelta

class TemplateGananciasPerdidasReport(models.Model):
	_name = 'template.ganancias.perdidas.report'
	_description = "Plantilla de Configuración del Estado de Ganancias-Pérdidas por Función"


	company_id = fields.Many2one('res.company',
		string="Compañia", 
		default=lambda self: self.env['res.company']._company_default_get('account.invoice'),
		domain = lambda self: [('id', 'in',[i.id for i in self.env['res.users'].browse(self.env.user.id).company_ids] )],readonly=True)

	name=fields.Char(string="Nombre",
		default="PLANTILLA DE CONFIGURACIÓN DEL ESTADO DE GANANCIAS-PÉRDIDAS POR FUNCIÓN")
	observations = fields.Char(string="Observaciones")


	#### Fecha hora
	fecha_hora = fields.Datetime(string="Fecha de Plantilla",default=datetime.now())

	###################### PLANTILLA DE CONFIGURACIÓN DE SUB-ELEMENTOS ######################

	ganancias_perdidas_rubro_ids = fields.One2many('template.ganancias.perdidas.rubro',
		'ganancias_perdidas_report_id',string="",ondelete="cascade")