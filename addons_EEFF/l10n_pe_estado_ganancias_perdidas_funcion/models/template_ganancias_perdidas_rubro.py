# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError


class TemplateGananciasPerdidasRubro(models.Model):
	_name = 'template.ganancias.perdidas.rubro'
	_description = "Plantilla de Configuración de Rubro Estado de Ganancias-Pérdidas por Función"
	_order = "nro_rubro asc"

	ganancias_perdidas_report_id = fields.Many2one("template.ganancias.perdidas.report", 
		string="Plantilla de Configuración Estado de Ganancias y Pérdidas por Función",readonly=True)

	operation_type = fields.Selection(selection=[('+','+'),('-','-'),('=','=')],string="Tipo de Operación")
	
	name=fields.Char(string="Nombre del Rubro",required=True)

	type_rubro = fields.Selection(selection=[('rubro','Rubro'),('rubro_padre','Rubro Padre')],
		string="Tipo de Rubro",default='rubro')

	calculation_type = fields.Selection(selection=[('accounts','Por Grupo de Cuentas Asociadas'),('manual','Manual')],
		string="Tipo de Cálculo",default='accounts',required=True)

	account_ids=fields.Many2many('account.account',string="Cuentas Asociadas al Rubro")
	movements_period = fields.Float(string="Movimientos del Ejercicio o Periodo")

	################### CAMPOS DE SUB-RUBRO #################
	nro_rubro = fields.Integer(string="N° Rubro")

	sub_rubro_line_ids = fields.One2many('template.ganancias.perdidas.sub.rubro','rubro_id',
		string="Sub-Rubros",ondelete="cascade")



