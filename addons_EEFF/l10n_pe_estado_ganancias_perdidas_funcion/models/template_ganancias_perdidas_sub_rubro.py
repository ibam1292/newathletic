# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError


class TemplateGananciasPerdidasSubRubro(models.Model):
	_name = 'template.ganancias.perdidas.sub.rubro'
	_description = "Plantilla de Configuración de Sub-Rubro Estado de Ganancias-Pérdidas por Función"
	_order = "nro_rubro asc , nro_sub_rubro asc"

	rubro_id = fields.Many2one("template.ganancias.perdidas.rubro", 
		string="Rubro de Estado de Ganancias y Pérdidas por Función",readonly=True)
	
	operation_type = fields.Selection(selection=[('+','+'),('-','-'),('=','=')],string="Tipo de Operación")
	
	name=fields.Char(string="Nombre del Sub-Rubro",required=True)

	calculation_type = fields.Selection(selection=[('accounts','Por Grupo de Cuentas Asociadas'),('manual','Manual')],
		string="Tipo de Cálculo",default='accounts',required=True)

	account_ids=fields.Many2many('account.account',string="Cuentas Asociadas al Sub-Rubro")
	movements_period = fields.Float(string="Movimientos del Ejercicio o Periodo")
	##############################################################################
	nro_rubro = fields.Integer(string="N° Rubro",related="rubro_id.nro_rubro",store=True,readonly=True)
	nro_sub_rubro = fields.Integer(string="N° Sub-Rubro")

