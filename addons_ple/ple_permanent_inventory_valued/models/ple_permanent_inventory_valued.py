import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from odoo.exceptions import UserError , ValidationError
from itertools import *

import logging
_logger=logging.getLogger(__name__)

options=[
	('in','esta en'),
	('not in','no esta en')
	]

class PlePermanentInventoryValued(models.Model):
	_name='ple.permanent.inventory.valued'
	_inherit='ple.base'
	_description = "Modulo PLE Registro Inventario Permanente Valorizado"

	ple_permanent_inventory_valued_line_ids=fields.One2many('ple.permanent.inventory.valued.line',
		'ple_permanent_inventory_valued_id',string="Libro Registro Inventario Permanente Valorizado",readonly=True,
		states={'draft': [('readonly', False)]})

	ple_permanent_inventory_physical_line_ids=fields.One2many('ple.permanent.inventory.valued.line',
		'ple_permanent_inventory_physical_id',string="Libro Registro Inventario Permanente Unidades Físicas",readonly=True,
		states={'draft': [('readonly', False)]})
	

	
	########################################### PARÁMETROS DE FILTRO DINÁMICO !!!
	dinamic_warehouse_ids= fields.Many2many('stock.warehouse',string="Almacenes")
	warehouse_option=fields.Selection(selection=options , string="")

	dinamic_location_ids= fields.Many2many('stock.location',string="Ubicaciones Origen")
	location_option=fields.Selection(selection=options , string="")

	dinamic_product_ids= fields.Many2many('product.product',string="Productos")
	product_option=fields.Selection(selection=options , string="")

	dinamic_partner_ids=fields.Many2many('res.partner',string="Proveedores/clientes")
	partner_option=fields.Selection(selection=options , string="")

	dinamic_categoria_ids=fields.Many2many('product.category',string="Categorías de Producto")
	categoria_option=fields.Selection(selection=options , string="")

	dinamic_date_infimo = fields.Datetime(string="Fecha Inicial")
	dinamic_date_supremo=fields.Datetime(string="Fecha Final")

	##############################################################################

	def button_view_tree_ple_permanent_inventory_valued_lines(self):
		self.ensure_one()
		view = self.env.ref('ple_permanent_inventory_valued.ple_permanent_inventory_valued_line_view_tree')
		if self.ple_permanent_inventory_valued_line_ids:
			diccionario = {
				'name': 'Libro PLE Inventario Permanente Valorizado',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'ple.permanent.inventory.valued.line',
				'view_id': view.id,
				'views': [(view.id,'tree')],
				'type': 'ir.actions.act_window',
				'domain': [('id', 'in', [i.id for i in self.ple_permanent_inventory_valued_line_ids] or [])],
				'context':{
					'search_default_filter_descripcion_existencia':1,
					}
			}
			return diccionario



	def button_view_tree_ple_permanent_inventory_physical_lines(self):
		self.ensure_one()
		view = self.env.ref('ple_permanent_inventory_valued.ple_permanent_inventory_physical_line_view_tree')
		if self.ple_permanent_inventory_physical_line_ids:
			diccionario = {
				'name': 'Libro PLE Inventario Permanente en Unidades',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'ple.permanent.inventory.valued.line',
				'view_id': view.id,
				'views': [(view.id,'tree')],
				'type': 'ir.actions.act_window',
				'domain': [('id', 'in', [i.id for i in self.ple_permanent_inventory_physical_line_ids] or [])],
				'context':{
					'search_default_filter_descripcion_existencia':1,
					}
			}
			return diccionario
	#################################################################################

	def open_wizard_print_form(self):
		res = super(PlePermanentInventoryValued,self).open_wizard_print_form()

		view = self.env.ref('ple_permanent_inventory_valued.view_wizard_printer_ple_permanent_inventory_valued_form')
		if view:

			return {
				'name': _('FORMULARIO DE IMPRESIÓN DEL LIBRO PLE'),
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'wizard.printer.ple.permanent.inventory.valued',
				'views': [(view.id,'form')],
				'view_id': view.id,
				'target': 'new',
				'context': {
					'default_ple_permanent_inventory_valued_id': self.id,
					'default_company_id' : self.company_id.id,}}

	
	def meses(self):
		dict_meses={}
		dict_meses['01']='Enero'
		dict_meses['02']='Febrero'
		dict_meses['03']='Marzo'
		dict_meses['04']='Abril'
		dict_meses['05']='Mayo'
		dict_meses['06']='Junio'
		dict_meses['07']='Julio'
		dict_meses['08']='Agosto'
		dict_meses['09']='Septiembre'
		dict_meses['10']='Octubre'
		dict_meses['11']='Noviembre'
		dict_meses['12']='Diciembre'
		return dict_meses

	
	##############################################################################################
	def get_query_kardex_saldo_inicial(self,filter_clause):
		query = """
			select 
	            table_quantity.product_id as product_id, 
	            table_quantity.product_template_id as product_tmpl_id, 
	            table_quantity.uom_code as uom_code, 
	            table_quantity.product_encoding_type_sunat as product_encoding_type_sunat, 
	            table_quantity.sunat_table_05_id as sunat_table_05_id, 
	            table_quantity.default_code as default_code, 
	            table_quantity.product_name as product_name,
	            table_quantity.company_id as company_id, 
	            table_quantity.quantity as quantity, 
	            table_quantity.qty_in as qty_in, 
	            table_quantity.cost_in as cost_in, 
	            table_quantity.total_cost_in as total_cost_in, 
	            table_quantity.qty_out as qty_out, 
	            table_quantity.cost_out as cost_out, 
	            table_quantity.total_cost_out as total_cost_out, 
	            table_quantity.cantidad_actual as qty, 
	            table_quantity.warehouse_id as warehouse_id, 
	            table_quantity.location_id as location_id, 

	            case when table_quantity.quantity < 0.00 then table_quantity.unit_cost
	            when coalesce(table_quantity.cantidad_actual,0.00) = 0.00 then table_quantity.unit_cost 
	            when coalesce(table_quantity.cantidad_actual,0.00) <> 0.00 
	            then coalesce(table_value.valor_actual,0.00)/coalesce(table_quantity.cantidad_actual,0.00) end unit_cost,

	            table_value.valor_actual as value, 
	            table_quantity.create_date as date 

	            from 

	            (select 
	            svl.id, 
	            svl.stock_move_id, 
	            case when svl.value >=0.00 then 'in'
	            else 'out' end type_operation,
	            sm.picking_id, 
	            rp.id as partner_id, 
	            svl.product_id as product_id, 
	            pdtp.id as product_template_id,
	            uu.code as uom_code,
	            st05.id as sunat_table_05_id, 
	            pdtp.product_encoding_type_sunat as product_encoding_type_sunat, 
	            pdtp.default_code as default_code, 
	            pdtp.name as product_name,
	            pdtp.categ_id as categ_id, 
	            sm.product_uom as uom_id,
	            svl.company_id as company_id,
	            svl.quantity,

	            case when svl.quantity >0.00 then svl.quantity
	            else 0.00
	            end qty_in,
	            case when svl.quantity >0.00 then svl.unit_cost
	            else 0.00
	            end cost_in,

	            case when svl.value > 0.00 then svl.value
	            when svl.value < 0.00 then 0.00
	            end total_cost_in,

	            case when svl.quantity <0.00 then abs(svl.quantity)
	            else 0.00
	            end qty_out,

	            case when svl.quantity <0.00 then abs(svl.unit_cost)
	            else 0.00
	            end cost_out,

	            case when svl.value < 0.00 then abs(svl.value)
	            when svl.value > 0.00 then 0.00
	            end total_cost_out,

	            svl.create_date, 
	            (select coalesce(sum(quantity),0.00) from stock_valuation_layer as svl2 where 
	              svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and svl2.id<svl.id order by svl.product_id,svl.create_date)
	              + coalesce(svl.quantity,0.00) as cantidad_actual,
	            svl.unit_cost, 
	            svl.value, 
	            sm.date,
	            sm.location_id as location_id, 
				sm.warehouse_id as warehouse_id 

	            from stock_valuation_layer as svl 
	            left join res_company rc on rc.id = svl.company_id 
	            left join stock_move sm on sm.id = svl.stock_move_id 
	            left join uom_uom uu on uu.id = sm.product_uom 
	            left join stock_picking stpck on stpck.id = sm.picking_id 
	            left join res_partner rp on rp.id = stpck.partner_id
	            left join stock_location sl on sl.id = sm.location_id 
	            left join account_move am on am.id = svl.account_move_id 
	            left join sale_order_line sol on sol.id = sm.sale_line_id
	            left join sale_order so on so.id = sol.order_id 
	            left join purchase_order_line pol on pol.id=sm.purchase_line_id 
	            left join purchase_order po on po.id = pol.order_id
	            left join product_product pdpd on pdpd.id = svl.product_id 
	            left join product_template pdtp on pdtp.id = pdpd.product_tmpl_id 
	            left join sunat_table_05 st05 on st05.id = pdtp.sunat_table_05_id
	            left join product_category pdct on pdct.id = pdtp.categ_id 
	            order by svl.product_id, svl.create_date, svl.id) as table_quantity

	            left join 

	            (select id,
	              (select coalesce(sum(value),0.00) from stock_valuation_layer as svl2 where 
	                svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and 
	                svl2.id<svl.id order by svl.product_id,svl.create_date)
	                + coalesce(svl.value,0.00) as valor_actual
	            from stock_valuation_layer as svl order by product_id, create_date,id) table_value on

	            table_value.id=table_quantity.id 
	            where table_quantity.create_date <'%s' %s """ %(
	            	self._get_star_date(),
	            	filter_clause or '')

		return query



	##############################################################################################

	def get_query_kardex(self,filter_clause):
		query = """
			select 
	            table_quantity.id as stock_valuation_layer_id,
	            table_quantity.stock_move_id as stock_move_id, 
	            table_quantity.account_move_id as account_move_id, 
	            table_quantity.asiento_contable, 
	            table_quantity.invoice_number as invoice_number, 
	            table_quantity.picking_id as picking_id, 
	            table_quantity.product_id as product_id, 
	            table_quantity.product_template_id as product_tmpl_id, 
	            table_quantity.uom_code as uom_code, 
	            table_quantity.product_encoding_type_sunat as product_encoding_type_sunat, 
	            table_quantity.sunat_table_05_id as sunat_table_05_id, 
	            table_quantity.default_code as default_code, 
	            table_quantity.product_name as product_name,
	            table_quantity.company_id as company_id, 
	            table_quantity.quantity as quantity, 
	            table_quantity.qty_in as qty_in, 
	            table_quantity.cost_in as cost_in, 
	            table_quantity.total_cost_in as total_cost_in, 
	            table_quantity.qty_out as qty_out, 
	            table_quantity.cost_out as cost_out, 
	            table_quantity.total_cost_out as total_cost_out, 
	            table_quantity.cantidad_actual as qty, 
	            table_quantity.warehouse_id as warehouse_id, 
	            table_quantity.location_id as location_id, 

	            case when table_quantity.quantity < 0.00 then table_quantity.unit_cost
	            when coalesce(table_quantity.cantidad_actual,0.00) = 0.00 then table_quantity.unit_cost 
	            when coalesce(table_quantity.cantidad_actual,0.00) <> 0.00 
	            then coalesce(table_value.valor_actual,0.00)/coalesce(table_quantity.cantidad_actual,0.00) end unit_cost,

	            table_value.valor_actual as value, 
	            table_quantity.create_date as date 

	            from 

	            (select 
	            svl.id, 
	            svl.stock_move_id, 
	            case when svl.value >=0.00 then 'in'
	            else 'out' end type_operation,
	            svl.account_move_id, 
	            case 
	            	when am.id is not NULL then am.name
	            	else 'AUX-999999' end asiento_contable,

	            CASE 
	                  WHEN sm.sale_line_id IS NOT NULL THEN
	                  (
	                  SELECT
	                    array_to_string( ARRAY_AGG ( account_move.name ORDER BY account_move.name ), ',' ) 
	                  FROM
	                    sale_order_line
	                    INNER JOIN sale_order_line_invoice_rel ON sale_order_line_invoice_rel.order_line_id = sale_order_line.
	                    ID INNER JOIN account_move_line ON sale_order_line_invoice_rel.invoice_line_id = account_move_line.
	                    ID INNER JOIN account_move ON account_move_line.move_id = account_move.ID 
	                  WHERE
	                    sale_order_line.id = sm.sale_line_id 
	                    AND account_move_line.product_id = sm.product_id  
	                  ) 
	                  WHEN sm.purchase_line_id IS NOT NULL THEN
	                  (
	                  SELECT
	                    array_to_string( ARRAY_AGG ( account_move.name ORDER BY account_move.name ), ',' ) 
	                  FROM
	                    purchase_order_line
	                    INNER JOIN account_move_line ON account_move_line.purchase_line_id = purchase_order_line.
	                    ID INNER JOIN account_move ON account_move_line.move_id = account_move.id 
	                  WHERE
	                    purchase_order_line.id = sm.purchase_line_id 
	                    AND account_move_line.product_id = sm.product_id 
	                  ) 
	                  WHEN sm.inventory_id IS NOT NULL THEN
	                  'AJUSTE' ELSE (
	                  SELECT
	                    array_to_string( ARRAY_AGG ( DISTINCT ( account_move.name ) ORDER BY account_move.name ), ',' ) 
	                  FROM
	                    account_move
	                    INNER JOIN account_move_line ON account_move_line.move_id = account_move.id 
	                    INNER JOIN stock_picking ON stock_picking.origin = account_move.name 
	                  WHERE
	                    stock_picking.ID = sm.picking_id 
	                    AND account_move_line.product_id = sm.product_id 
	                  ) 
	                END AS invoice_number,
	            sm.picking_id, 
	            rp.id as partner_id, 
	            svl.product_id as product_id, 
	            pdtp.id as product_template_id,
	            uu.code as uom_code,
	            st05.id as sunat_table_05_id, 
	            pdtp.product_encoding_type_sunat as product_encoding_type_sunat, 
	            pdtp.default_code as default_code, 
	            pdtp.name as product_name,
	            pdtp.categ_id as categ_id, 
	            sm.product_uom as uom_id,
	            svl.company_id as company_id,
	            svl.quantity,

	            case when svl.quantity >0.00 then svl.quantity
	            else 0.00
	            end qty_in,
	            case when svl.quantity >0.00 then svl.unit_cost
	            else 0.00
	            end cost_in,

	            case when svl.value > 0.00 then svl.value
	            when svl.value < 0.00 then 0.00
	            end total_cost_in,

	            case when svl.quantity <0.00 then abs(svl.quantity)
	            else 0.00
	            end qty_out,

	            case when svl.quantity <0.00 then abs(svl.unit_cost)
	            else 0.00
	            end cost_out,

	            case when svl.value < 0.00 then abs(svl.value)
	            when svl.value > 0.00 then 0.00
	            end total_cost_out,

	            svl.create_date, 
	            (select coalesce(sum(quantity),0.00) from stock_valuation_layer as svl2 where 
	              svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and svl2.id<svl.id order by svl.product_id,svl.create_date)
	              + coalesce(svl.quantity,0.00) as cantidad_actual,
	            svl.unit_cost, 
	            svl.value, 
	            sm.date,
	            sm.location_id as location_id, 
				sm.warehouse_id as warehouse_id 

	            from stock_valuation_layer as svl 
	            left join res_company rc on rc.id = svl.company_id 
	            left join stock_move sm on sm.id = svl.stock_move_id 
	            left join uom_uom uu on uu.id = sm.product_uom 
	            left join stock_picking stpck on stpck.id = sm.picking_id 
	            left join res_partner rp on rp.id = stpck.partner_id
	            left join stock_location sl on sl.id = sm.location_id 
	            left join account_move am on am.id = svl.account_move_id 
	            left join sale_order_line sol on sol.id = sm.sale_line_id
	            left join sale_order so on so.id = sol.order_id 
	            left join purchase_order_line pol on pol.id=sm.purchase_line_id 
	            left join purchase_order po on po.id = pol.order_id
	            left join product_product pdpd on pdpd.id = svl.product_id 
	            left join product_template pdtp on pdtp.id = pdpd.product_tmpl_id 
	            left join sunat_table_05 st05 on st05.id = pdtp.sunat_table_05_id
	            left join product_category pdct on pdct.id = pdtp.categ_id 
	            order by svl.product_id, svl.create_date, svl.id) as table_quantity

	            left join 

	            (select id,
	              (select coalesce(sum(value),0.00) from stock_valuation_layer as svl2 where 
	                svl2.create_date <= svl.create_date and svl.product_id =svl2.product_id and 
	                svl2.id<svl.id order by svl.product_id,svl.create_date)
	                + coalesce(svl.value,0.00) as valor_actual
	            from stock_valuation_layer as svl order by product_id, create_date,id) table_value on

	            table_value.id=table_quantity.id 
	            where table_quantity.create_date <='%s' %s """ %(self._get_end_date(),filter_clause or '')

		return query

	
	def _get_order_print(self , object):
		total=''
		if self.print_order == 'date': # ORDENAMIENTO POR LA FECHA CONTABLE
			total=sorted(object,
				key=lambda PlePermanentInventoryValuedLine:PlePermanentInventoryValuedLine.fecha_movimiento)
		elif self.print_order == 'nro_documento':
			total=sorted(object,
				key=lambda PlePermanentInventoryValuedLine:PlePermanentInventoryValuedLine.asiento_contable)
		
		return total



	def _get_datas(self):

		filter_clause = ""

		warehouses=tuple(self.dinamic_warehouse_ids.mapped('id'))
		len_warehouses = len(warehouses or '')
		if len_warehouses:
			filter_clause += " and table_quantity.warehouse_id %s %s" % (self.warehouse_option or 'in',
				str(warehouses) if len_warehouses!=1 else str(warehouses)[0:len(str(warehouses))-2] + ')')


		locations=tuple(self.dinamic_location_ids.mapped('id'))
		len_locations = len(locations or '')
		if len_locations:
			filter_clause += " and table_quantity.location_id %s %s" % (self.location_option or 'in',
				str(locations) if len_locations!=1 else str(locations)[0:len(str(locations))-2] + ')')


		partners=tuple(self.dinamic_partner_ids.mapped('id'))
		len_partners = len(partners or '')
		if len_partners:
			filter_clause += " and table_quantity.partner_id %s %s" % (self.partner_option or 'in',
				str(partners) if len_partners!=1 else str(partners)[0:len(str(partners))-2] + ')')


		products=tuple(self.dinamic_product_ids.mapped('id'))
		len_products = len(products or '')
		if len_products:
			filter_clause += " and table_quantity.product_id %s %s" % (self.product_option or 'in',
				str(products) if len_products!=1 else str(products)[0:len(str(products))-2] + ')')


		categorias=tuple(self.dinamic_categoria_ids.mapped('id'))
		len_categorias = len(categorias or '')
		if len_categorias:
			filter_clause += " and table_quantity.categ_id %s %s" % (self.categoria_option or 'in',
				str(categorias) if len_categorias!=1 else str(categorias)[0:len(str(categorias))-2] + ')')


		query = self.get_query_kardex(filter_clause)

		self.env.cr.execute(query)
		records = self.env.cr.dictfetchall()
		
		return records
	###############################################################################



	def _periodo_fiscal(self):
		periodo = "%s%s00" % (self.fiscal_year or 'YYYY', self.fiscal_month or 'MM')
		return periodo


	
	def generar_libro(self):
		
		self.state='open'
		self.ple_permanent_inventory_valued_line_ids.unlink()
		self.ple_permanent_inventory_physical_line_ids.unlink()
		
		registro= []

		####### AGREGANDO PRIMERO LOS SALDOS INICIALES #######
		lista_movimientos = self._get_datas()

		grupos_de_productos = groupby(lista_movimientos,lambda x : x['product_id'] )
		
		diccionario_productos = {}

		for k , v in grupos_de_productos:
			diccionario_productos[k] = list(v)
		
		
		for conjunto in diccionario_productos:
			max_date = max([i['date'] for i in diccionario_productos[conjunto]])

			if max_date.strftime('%Y-%m-%d') < self._get_star_date():
				line = diccionario_productos[conjunto][len(diccionario_productos[conjunto])-1]

				registro.append((0,0,{
					'stock_valuation_layer_id': False,
					'stock_move_id': False,
					'periodo':self._periodo_fiscal() or False,
					'product_id': line['product_id'] or False,
					'picking_id': False,
					'location_id': False,
					'warehouse_id': False,
					'move_id': False,
					'asiento_contable': '',
					'fecha_movimiento': self._get_star_date(), 
					'invoice_number' : '',
					'm_correlativo_asiento_contable': "" or False,
					'codigo_establecimiento_anexo': (self.company_id.vat or "")[:4] or "9999" or False,
					'codigo_catalogo_utilizado': line['product_encoding_type_sunat'] or '',
					'tipo_existencia': line['sunat_table_05_id'] or False,
					'codigo_propio_existencia': line['default_code'] or '',
					'codigo_existencia_catalogo_OSCE': "" or '', 
					'tipo_operacion_efectuada': "16" or False, 
					'descripcion_existencia': line['product_name'] or '', 
					'codigo_unidad_medida': line['uom_code'] or '', 
					'cantidad_unidades_fisicas_ingresado': 0.00,
					'costo_unitario_bien_ingresado': 0.00,
					'costo_total_bien_ingresado': 0.00,
					'cantidad_unidades_fisicas_retirado': 0.00, 
					'costo_unitario_bien_retirado': 0.00,
					'costo_total_bien_retirado': 0.00, 
					'cantidad_unids_fisicas_saldo_final': line['qty'] or 0.00, 
					'costo_unitario_saldo_final': line['unit_cost'] or 0.00, 
					'costo_total_saldo_final': line['value'] or 0.00, 
					}))
			else:

				min_date = min([i['date'] for i in diccionario_productos[conjunto]])

				if min_date.strftime('%Y-%m-%d') < self._get_star_date():

					indicador = 0
					for j in diccionario_productos[conjunto]:
						if j['date'].strftime('%Y-%m-%d') < self._get_star_date():
							indicador += 1
						else:
							break

					indicador -= 1

					line = diccionario_productos[conjunto][indicador]

					registro.append((0,0,{
						'stock_valuation_layer_id': False,
						'stock_move_id': False,
						'periodo':self._periodo_fiscal() or False,
						'product_id': line['product_id'] or False,
						'picking_id': False,
						'location_id': False,
						'warehouse_id': False,
						'move_id': False,
						'asiento_contable': '',
						'fecha_movimiento': self._get_star_date(), 
						'invoice_number' : '',
						'm_correlativo_asiento_contable': "" or False,
						'codigo_establecimiento_anexo': (self.company_id.vat or "")[:4] or "9999" or False,
						'codigo_catalogo_utilizado': line['product_encoding_type_sunat'] or '',
						'tipo_existencia': line['sunat_table_05_id'] or False,
						'codigo_propio_existencia': line['default_code'] or '',
						'codigo_existencia_catalogo_OSCE': "" or '', 
						'tipo_operacion_efectuada': "16" or False, 
						'descripcion_existencia': line['product_name'] or '', 
						'codigo_unidad_medida': line['uom_code'] or '', 
						'cantidad_unidades_fisicas_ingresado': 0.00,
						'costo_unitario_bien_ingresado': 0.00,
						'costo_total_bien_ingresado': 0.00,
						'cantidad_unidades_fisicas_retirado': 0.00, 
						'costo_unitario_bien_retirado': 0.00,
						'costo_total_bien_retirado': 0.00, 
						'cantidad_unids_fisicas_saldo_final': line['qty'] or 0.00, 
						'costo_unitario_saldo_final': line['unit_cost'] or 0.00, 
						'costo_total_saldo_final': line['value'] or 0.00, 
					}))


					for line in diccionario_productos[conjunto]:
						if line['date'].strftime('%Y-%m-%d') >= self._get_star_date():

							registro.append((0,0,{
								'stock_valuation_layer_id': line['stock_valuation_layer_id'] or False,
								'stock_move_id':line['stock_move_id'] or False,
								'periodo':self._periodo_fiscal() or False,
								'product_id': line['product_id'] or False,
								'picking_id':line['picking_id'] or False,
								'location_id':line['location_id'] or False,
								'warehouse_id':line['warehouse_id'] or False,
								'move_id': line['account_move_id'] or False,
								'asiento_contable': line['asiento_contable'] or 'AUX-999999',
								'fecha_movimiento' : line['date'] or False, 
								'invoice_number' : line['invoice_number'] or '',
								'm_correlativo_asiento_contable': "M1" or False,
								'codigo_establecimiento_anexo': (self.company_id.vat or "")[:4] or "9999" or False,
								'codigo_catalogo_utilizado': line['product_encoding_type_sunat'] or '',
								'tipo_existencia': line['sunat_table_05_id'] or False,
								'codigo_propio_existencia': line['default_code'] or '',
								'codigo_existencia_catalogo_OSCE': "" or '', 
								'tipo_operacion_efectuada': "" or False, 
								'descripcion_existencia': line['product_name'] or '', 
								'codigo_unidad_medida': line['uom_code'] or '', 
								'cantidad_unidades_fisicas_ingresado': line['qty_in'] or 0.00,
								'costo_unitario_bien_ingresado': line['cost_in'] or 0.00,
								'costo_total_bien_ingresado': line['total_cost_in'] or 0.00,
								'cantidad_unidades_fisicas_retirado':line['qty_out'] or 0.00, 
								'costo_unitario_bien_retirado': line['cost_out'] or 0.00,
								'costo_total_bien_retirado': line['total_cost_out'] or 0.00, 
								'cantidad_unids_fisicas_saldo_final': line['qty'] or 0.00, 
								'costo_unitario_saldo_final': line['unit_cost'] or 0.00, 
								'costo_total_saldo_final': line['value'] or 0.00, 
							}))

				else:
					line= diccionario_productos[conjunto][0]					
					registro.append((0,0,{
						'stock_valuation_layer_id': False,
						'stock_move_id': False,
						'periodo':self._periodo_fiscal() or False,
						'product_id': line['product_id'] or False,
						'picking_id': False,
						'location_id': False,
						'warehouse_id': False,
						'move_id': False,
						'asiento_contable': '',
						'fecha_movimiento': self._get_star_date(), 
						'invoice_number' : '',
						'm_correlativo_asiento_contable': "" or False,
						'codigo_establecimiento_anexo': (self.company_id.vat or "")[:4] or "9999" or False,
						'codigo_catalogo_utilizado': line['product_encoding_type_sunat'] or '',
						'tipo_existencia': line['sunat_table_05_id'] or False,
						'codigo_propio_existencia': line['default_code'] or '',
						'codigo_existencia_catalogo_OSCE': "" or '', 
						'tipo_operacion_efectuada': "16" or False, 
						'descripcion_existencia': line['product_name'] or '', 
						'codigo_unidad_medida': line['uom_code'] or '', 
						'cantidad_unidades_fisicas_ingresado': 0.00,
						'costo_unitario_bien_ingresado': 0.00,
						'costo_total_bien_ingresado': 0.00,
						'cantidad_unidades_fisicas_retirado': 0.00, 
						'costo_unitario_bien_retirado': 0.00,
						'costo_total_bien_retirado': 0.00, 
						'cantidad_unids_fisicas_saldo_final': 0.00, 
						'costo_unitario_saldo_final': 0.00, 
						'costo_total_saldo_final': 0.00, 
					}))

					for line in diccionario_productos[conjunto]:
						registro.append((0,0,{
							'stock_valuation_layer_id': line['stock_valuation_layer_id'] or False,
							'stock_move_id':line['stock_move_id'] or False,
							'periodo':self._periodo_fiscal() or False,
							'product_id': line['product_id'] or False,
							'picking_id':line['picking_id'] or False,
							'location_id':line['location_id'] or False,
							'warehouse_id':line['warehouse_id'] or False,
							'move_id': line['account_move_id'] or False,
							'asiento_contable': line['asiento_contable'] or 'AUX-999999',
							'fecha_movimiento' : line['date'] or False, 
							'invoice_number' : line['invoice_number'] or '',
							'm_correlativo_asiento_contable': "M1" or False,
							'codigo_establecimiento_anexo': (self.company_id.vat or "")[:4] or "9999" or False,
							'codigo_catalogo_utilizado': line['product_encoding_type_sunat'] or '',
							'tipo_existencia': line['sunat_table_05_id'] or False,
							'codigo_propio_existencia': line['default_code'] or '',
							'codigo_existencia_catalogo_OSCE': "" or '', 
							'tipo_operacion_efectuada': "" or False, 
							'descripcion_existencia': line['product_name'] or '', 
							'codigo_unidad_medida': line['uom_code'] or '', 
							'cantidad_unidades_fisicas_ingresado': line['qty_in'] or 0.00,
							'costo_unitario_bien_ingresado': line['cost_in'] or 0.00,
							'costo_total_bien_ingresado': line['total_cost_in'] or 0.00,
							'cantidad_unidades_fisicas_retirado':line['qty_out'] or 0.00, 
							'costo_unitario_bien_retirado': line['cost_out'] or 0.00,
							'costo_total_bien_retirado': line['total_cost_out'] or 0.00, 
							'cantidad_unids_fisicas_saldo_final': line['qty'] or 0.00, 
							'costo_unitario_saldo_final': line['unit_cost'] or 0.00, 
							'costo_total_saldo_final': line['value'] or 0.00, 
						}))


		self.ple_permanent_inventory_valued_line_ids = registro
		self.ple_permanent_inventory_physical_line_ids = self.ple_permanent_inventory_valued_line_ids



	def _convert_object_date(self, date):
		# parametro date que retorna un valor vacio o el foramto 01/01/2100 dia/mes/año
		if date:
			return date.strftime('%d/%m/%Y')
		else:
			return ''