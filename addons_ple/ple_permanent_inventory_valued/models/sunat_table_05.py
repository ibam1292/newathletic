from odoo import models,api,fields


class SunatTable05(models.Model):
    _name = "sunat.table.05"
    _description = "Tabla 5 : Tipo de Existencia"

    active = fields.Boolean("Activo",default=True)
    name = fields.Char("Descripción")
    code = fields.Char("Código")