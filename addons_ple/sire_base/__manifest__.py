{
	'name': 'SUNAT SIRE Base',
	'version': "1.0.2",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends':['account','report_formats'],
	'description':'''
		Modulo Base SIRE SUNAT.
		> 
		''',
	'data':[
		'security/ir.model.access.csv',
		'views/sire_base_view.xml',
		'views/res_company_view.xml',

	],
	'installable': True,
    'auto_install': False,
}