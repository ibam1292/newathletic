{
	'name': 'SIRE SUNAT VENTAS',
	'summary': 'SIRE SUNAT VENTAS Perú',
	'version': "1.1.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends':['account','sire_base','gestionit_pe_fe'],
	'description':'''
		Modulo SIRE SUNAT VENTAS
	''',
	'data':[
		'security/ir.model.access.csv',
		'views/sire_sale_view.xml',
	],
	'installable': True,
    'auto_install': False,
}